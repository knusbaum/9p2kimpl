#include <stdint.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fcall.h"
#include "reader.h"
#include "9types.h"

void parse_TRversion(FCall *fc, uint8_t *messagep);
void parse_Tauth(FCall *fc, uint8_t *messagep);
void parse_Rauth(FCall *fc, uint8_t *messagep);
void parse_Tread(FCall *fc, uint8_t *messagep);
void parse_Rread(FCall *fc, uint8_t *messagep);
void parse_Tattach(FCall *fc, uint8_t *messagep);
void parse_Rattach(FCall *fc, uint8_t *messagep);
void parse_Tstat(FCall *fc, uint8_t *messagep);
void parse_Rstat(FCall *fc, uint8_t *messagep);
void parse_Topen(FCall *fc, uint8_t *messagep);
void parse_Ropen(FCall *fc, uint8_t *messagep);
void parse_Twalk(FCall *fc, uint8_t *messagep);
void parse_Rwalk(FCall *fc, uint8_t *messagep);
void parse_Rerror(FCall *fc, uint8_t *messagep);
void parse_Tclunk(FCall *fc, uint8_t *messagep);
void parse_Tcreate(FCall *fc, uint8_t *messagep);
void parse_Rcreate(FCall *fc, uint8_t *messagep);
void parse_Twstat(FCall *fc, uint8_t *messagep);
void parse_Twrite(FCall *fc, uint8_t *messagep);
void parse_Tremove(FCall *fc, uint8_t *messagep);

int readbytes(int sockfd, ssize_t bytes, uint8_t *buffer) {
    ssize_t read = 0;
    while(read < bytes) {
        int justread = recv(sockfd, buffer + read, bytes - read, 0);
        if(justread == -1) {
            perror("Failed to read from socket.");
            return 0;
        }
        else if(justread == 0) {
            printf("Remote host closed connection.\n");
            return 0;
        }
        read += justread;
    }

    return 1;
}

FCall *parse_call(int sockfd) {
    uint8_t sizebuff[4];
    if(!readbytes(sockfd, 4, sizebuff)) {
        return NULL;
    }
    uint32_t size = fromLittleE32(sizebuff, NULL);

    uint8_t message[size];
    uint8_t *messagep = message;
    if(!readbytes(sockfd, size - 4, message)) {
        return NULL;
    }

    FCall *fc = malloc(sizeof *fc);
    memset(fc, 0, sizeof *fc);

    fc->type = *messagep++;
    fc->tag = fromLittleE16(messagep, &messagep);

    switch(fc->type) {
    case Tversion:
    case Rversion:
        parse_TRversion(fc, messagep);
        break;
    case Tauth:
        parse_Tauth(fc, messagep);
        break;
    case Rauth:
        parse_Rauth(fc, messagep);
        break;
    case Tread:
        parse_Tread(fc, messagep);
        break;
    case Rread:
        parse_Rread(fc, messagep);
        break;
    case Tattach:
        parse_Tattach(fc, messagep);
        break;
    case Rattach:
        parse_Rattach(fc, messagep);
        break;
    case Tstat:
        parse_Tstat(fc, messagep);
        break;
    case Rstat:
        parse_Rstat(fc, messagep);
        break;
    case Topen:
        parse_Topen(fc, messagep);
        break;
    case Ropen:
        parse_Ropen(fc, messagep);
        break;
    case Twalk:
        parse_Twalk(fc, messagep);
        break;
    case Rwalk:
        parse_Rwalk(fc, messagep);
        break;
    case Tclunk:
        parse_Tclunk(fc, messagep);
        break;
    case Tcreate:
        parse_Tcreate(fc, messagep);
        break;
    case Rcreate:
        parse_Rcreate(fc, messagep);
        break;
    case Twstat:
        parse_Twstat(fc, messagep);
        break;
    case Twrite:
        parse_Twrite(fc, messagep);
        break;
    case Rclunk:
        break;
    case Tremove:
        parse_Tremove(fc, messagep);
        break;
    case Rerror:
        parse_Rerror(fc, messagep);
        break;
    default:
        printf("Parser for type %s unimplemented.\n", string_for_type(fc->type));
        free(fc);
        return NULL;
        break;
    }
    return fc;
}

void parse_TRversion(FCall *fc, uint8_t *messagep) {
    fc->TRversion.msize = fromLittleE32(messagep, &messagep);
    fc->TRversion.version = readStrAlloc(messagep, &messagep);
}

void parse_Tauth(FCall *fc, uint8_t *messagep) {
    fc->Tauth.afid = fromLittleE32(messagep, &messagep);
    fc->Tauth.uname = readStrAlloc(messagep, &messagep);
    fc->Tauth.aname = readStrAlloc(messagep, &messagep);
}

void parse_Rauth(FCall *fc, uint8_t *messagep) {
    fc->Rauth.aqid = readQid(messagep, NULL);
}

void parse_Tread(FCall *fc, uint8_t *messagep) {
    fc->Tread.fid = fromLittleE32(messagep, &messagep);
    fc->Tread.offset = fromLittleE64(messagep, &messagep);
    fc->Tread.count = fromLittleE32(messagep, &messagep);
}

void parse_Rread(FCall *fc, uint8_t *messagep) {
    fc->Rread.count = fromLittleE32(messagep, &messagep);
    uint8_t *data = malloc(fc->Rread.count);
    memcpy(data, messagep, fc->Rread.count);
    fc->Rread.data = data;
}

void parse_Tattach(FCall *fc, uint8_t *messagep) {
    fc->Tattach.fid = fromLittleE32(messagep, &messagep);
    fc->Tattach.afid = fromLittleE32(messagep, &messagep);
    fc->Tattach.uname = readStrAlloc(messagep, &messagep);
    fc->Tattach.aname = readStrAlloc(messagep, &messagep);
}

void parse_Rattach(FCall *fc, uint8_t *messagep) {
    fc->Rattach.qid = readQid(messagep, NULL);
}

RStats parse_stats(uint8_t *buffer, uint32_t count) {
    RStats rs;
    rs.stat_count = 0;
    rs.stats = NULL;

    uint8_t *current = buffer;
    uint16_t statcount = 0;
    uint16_t alloccount = 8;
    RStat *statsp = malloc(sizeof (RStat) * alloccount);
    if(!statsp) return rs;

    while(current < buffer + count) {
        if(statcount == alloccount) {
            alloccount *= 2;
            statsp = realloc(statsp, sizeof (RStat) * alloccount);
            if(!statsp) return rs;
        }
        parse_stat(statsp + statcount, current, &current);
        statcount++;
    }
    rs.stat_count = statcount;
    rs.stats = statsp;
    return rs;
}

void free_rstats(RStats stats) {
    for(size_t i = 0; i < stats.stat_count; i++) {
        free_rstat(stats.stats + i);
    }
    free(stats.stats);
}

uint16_t stat_length(RStat *rs) {
// size[2], type[2], dev[4], qid[13], mode[4], atime[4], mtime[4], length[8],
// name[s], uid[s], gid[s], muid[s]
    return 2 + 2 + 4 + 13 + 4 + 4 + 4 + 8
        + (2 + strlen(rs->name))
        + (2 + strlen(rs->uid))
        + (2 + strlen(rs->gid))
        + (2 + strlen(rs->muid));
}

void write_rstat(RStat *stat, uint8_t *buffer, uint8_t **newbuffer) {
    uint16_t len = stat_length(stat) - 2;
    toLittleE16(len, buffer, &buffer);
    toLittleE16(stat->type, buffer, &buffer);
    toLittleE32(stat->dev, buffer, &buffer);
    *(buffer++) = stat->qid.type;
    toLittleE32(stat->qid.vers, buffer, &buffer);
    toLittleE64(stat->qid.uid, buffer, &buffer);
    toLittleE32(stat->mode, buffer, &buffer);
    toLittleE32(stat->atime, buffer, &buffer);
    toLittleE32(stat->mtime, buffer, &buffer);
    toLittleE64(stat->length, buffer, &buffer);
    buffer = writeStr(stat->name, buffer);
    buffer = writeStr(stat->uid, buffer);
    buffer = writeStr(stat->gid, buffer);
    buffer = writeStr(stat->muid, buffer);
    if(newbuffer) *newbuffer = buffer;
}

void parse_stat(RStat *stat, uint8_t *messagep, uint8_t **newmessage) {
    // throw away the size
    fromLittleE16(messagep, &messagep);
    stat->type = fromLittleE16(messagep, &messagep);
    stat->dev = fromLittleE32(messagep, &messagep);
    stat->qid.type = *messagep++;
    stat->qid.vers = fromLittleE32(messagep, &messagep);
    stat->qid.uid = fromLittleE64(messagep, &messagep);
    stat->mode = fromLittleE32(messagep, &messagep);
    stat->atime = fromLittleE32(messagep, &messagep);
    stat->mtime = fromLittleE32(messagep, &messagep);
    stat->length = fromLittleE64(messagep, &messagep);
    stat->name = readStrAlloc(messagep, &messagep);
    stat->uid = readStrAlloc(messagep, &messagep);
    stat->gid = readStrAlloc(messagep, &messagep);
    stat->muid = readStrAlloc(messagep, &messagep);
    if(newmessage) *newmessage = messagep;
}

void parse_Tstat(FCall *fc, uint8_t *messagep) {
    fc->Tstat.fid = fromLittleE32(messagep, &messagep);
}

void parse_Rstat(FCall *fc, uint8_t *messagep) {
    //throw away the data length
    fromLittleE16(messagep, &messagep);
    parse_stat(&fc->Rstat, messagep, NULL);
}

void parse_Topen(FCall *fc, uint8_t *messagep) {
    fc->Topen.fid = fromLittleE32(messagep, &messagep);
    fc->Topen.mode = *(messagep++);
}

void parse_Ropen(FCall *fc, uint8_t *messagep) {
    fc->Ropen.qid = readQid(messagep, &messagep);
    fc->Ropen.iounit = fromLittleE32(messagep, &messagep);
}

void parse_Twalk(FCall *fc, uint8_t *messagep) {
    fc->Twalk.fid = fromLittleE32(messagep, &messagep);
    fc->Twalk.newfid = fromLittleE32(messagep, &messagep);
    fc->Twalk.nwname = fromLittleE16(messagep, &messagep);
    for(uint16_t i = 0; i < fc->Twalk.nwname; i++) {
        fc->Twalk.wname[i] = readStrAlloc(messagep, &messagep);
    }
}

void parse_Rwalk(FCall *fc, uint8_t *messagep) {
    fc->Rwalk.nwqid = fromLittleE16(messagep, &messagep);
    for(int i = 0; i < fc->Rwalk.nwqid; i++) {
        fc->Rwalk.wqid[i] = readQid(messagep, &messagep);
    }
}

void parse_Rerror(FCall *fc, uint8_t *messagep) {
    fc->Rerror.ename = readStrAlloc(messagep, &messagep);
}

void parse_Tclunk(FCall *fc, uint8_t *messagep) {
    fc->Tclunk.fid = fromLittleE32(messagep, &messagep);
}

void parse_Tcreate(FCall *fc, uint8_t *messagep) {
    fc->Tcreate.fid = fromLittleE32(messagep, &messagep);
    fc->Tcreate.name = readStrAlloc(messagep, &messagep);
    fc->Tcreate.perm = fromLittleE32(messagep, &messagep);
    fc->Tcreate.mode = *messagep++;
}

void parse_Rcreate(FCall *fc, uint8_t *messagep) {
    fc->Rcreate.qid = readQid(messagep, &messagep);
    fc->Rcreate.iounit = fromLittleE32(messagep, &messagep);
}

void parse_Twstat(FCall *fc, uint8_t *messagep) {
    fc->Twstat.fid = fromLittleE32(messagep, &messagep);
    // Throw away length.
    fromLittleE16(messagep, &messagep);
    //fc->Twstat.stat = malloc(sizeof (RStat) * fc->Twstat.stat_n);
    parse_stat(&fc->Twstat.stat, messagep, &messagep);
}

void parse_Twrite(FCall *fc, uint8_t *messagep) {
    fc->Twrite.fid = fromLittleE32(messagep, &messagep);
    fc->Twrite.offset = fromLittleE64(messagep, &messagep);
    fc->Twrite.count = fromLittleE32(messagep, &messagep);
    fc->Twrite.data = readBytesAlloc(fc->Twrite.count, messagep, &messagep);
}

void parse_Tremove(FCall *fc, uint8_t *messagep) {
    fc->Tremove.fid = fromLittleE32(messagep, &messagep);
}

int write_TRversion(int sockfd, FCall *fc);
int write_Tauth(int sockfd, FCall *fc);
int write_Rauth(int sockfd, FCall *fc);
int write_Rerror(int sockfd, FCall *fc);
int write_Tflush(int sockfd, FCall *fc);
int write_Tattach(int sockfd, FCall *fc);
int write_Rattach(int sockfd, FCall *fc);
int write_Twalk(int sockfd, FCall *fc);
int write_Rwalk(int sockfd, FCall *fc);
int write_Topen(int sockfd, FCall *fc);
int write_Ropen(int sockfd, FCall *fc);
int write_Tcreate(int sockfd, FCall *fc);
int write_Rcreate(int sockfd, FCall *fc);
int write_Tread(int sockfd, FCall *fc);
int write_Rread(int sockfd, FCall *fc);
int write_Twrite(int sockfd, FCall *fc);
int write_Rwrite(int sockfd, FCall *fc);
int write_Tclunk(int sockfd, FCall *fc);
int write_Rclunk(int sockfd, FCall *fc);
int write_Tremove(int sockfd, FCall *fc);
int write_Tstat(int sockfd, FCall *fc);
int write_Rstat(int sockfd, FCall *fc);
int write_Twstat(int sockfd, FCall *fc);
int write_Rwstat(int sockfd, FCall *fc);
int write_Rremove(int sockfd, FCall *fc);

int write_fcall(int sockfd, FCall *fc) {
    switch(fc->type) {
    case Tversion:
    case Rversion:
        return write_TRversion(sockfd, fc);
        break;
    case Tauth:
        return write_Tauth(sockfd, fc);
        break;
    case Rauth:
        return write_Rauth(sockfd, fc);
        break;
    case Rerror:
        return write_Rerror(sockfd, fc);
        break;
    case Tflush:
        return write_Tflush(sockfd, fc);
        break;
    case Tattach:
        return write_Tattach(sockfd, fc);
        break;
    case Rattach:
        return write_Rattach(sockfd, fc);
        break;
    case Twalk:
        return write_Twalk(sockfd, fc);
        break;
    case Rwalk:
        return write_Rwalk(sockfd, fc);
        break;
    case Topen:
        return write_Topen(sockfd, fc);
        break;
    case Ropen:
        return write_Ropen(sockfd, fc);
        break;
    case Tcreate:
        return write_Tcreate(sockfd, fc);
        break;
    case Rcreate:
        return write_Rcreate(sockfd, fc);
        break;
    case Tread:
        return write_Tread(sockfd, fc);
        break;
    case Rread:
        return write_Rread(sockfd, fc);
        break;
    case Twrite:
        return write_Twrite(sockfd, fc);
        break;
    case Rwrite:
        return write_Rwrite(sockfd, fc);
        break;
    case Tclunk:
        return write_Tclunk(sockfd, fc);
        break;
    case Rclunk:
        return write_Rclunk(sockfd, fc);
        break;
    case Tremove:
        return write_Tremove(sockfd, fc);
        break;
    case Rremove:
        return write_Rremove(sockfd, fc);
        break;
    case Tstat:
        return write_Tstat(sockfd, fc);
        break;
    case Rstat:
        return write_Rstat(sockfd, fc);
        break;
    case Twstat:
        return write_Twstat(sockfd, fc);
        break;
    case Rwstat:
        return write_Rwstat(sockfd, fc);
        break;
    default:
        return 0;
    }
    return 1;
}

int write_TRversion(int sockfd, FCall *fc) {
    // size[4] Tversion tag[2] msize[4] version[s]
    uint16_t strl = strlen(fc->TRversion.version);
    uint32_t len = 4 + 1 + 2 + 4 + (2 + strl);
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;
    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = fc->type;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->TRversion.msize, buffptr, &buffptr);
    buffptr = writeStr(fc->TRversion.version, buffptr);
    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}
int write_Tauth(int sockfd, FCall *fc) {
    return 0;
}
int write_Rauth(int sockfd, FCall *fc) {
    return 0;
}
int write_Rerror(int sockfd, FCall *fc) {
    uint16_t errl = strlen(fc->Rerror.ename);
    uint32_t len = 4 + 1 + 2 + (2 + errl);
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rerror;
    toLittleE16(fc->tag, buffptr, &buffptr);
    buffptr = writeStr(fc->Rerror.ename, buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }

    return 1;
}
int write_Tflush(int sockfd, FCall *fc) {
    return 0;
}
int write_Tattach(int sockfd, FCall *fc) {
// size[4] Tattach tag[2] fid[4] afid[4] uname[s] aname[s]
    uint16_t unamel = strlen(fc->Tattach.uname);
    uint16_t anamel = strlen(fc->Tattach.aname);

    uint32_t len = 4 + 1 + 2 + 4 + 4 + (2 + unamel) + (2 + anamel);
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Tattach;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->Tattach.fid, buffptr, &buffptr);
    toLittleE32(fc->Tattach.afid, buffptr, &buffptr);
    buffptr = writeStr(fc->Tattach.uname, buffptr);
    buffptr = writeStr(fc->Tattach.aname, buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}
int write_Rattach(int sockfd, FCall *fc) {
// size[4] Rattach tag[2] qid[13]
    uint32_t len = 4 + 1 + 2 + 13;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;
    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rattach;
    toLittleE16(fc->tag, buffptr, &buffptr);
    *buffptr++ = fc->Rattach.qid.type;
    toLittleE32(fc->Rattach.qid.vers, buffptr, &buffptr);
    toLittleE64(fc->Rattach.qid.uid, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}
int write_Twalk(int sockfd, FCall *fc) {
// size[4] Twalk  tag[2] fid[4] newfid[4] nwname[2] nwname*(wname[s])
    uint32_t len = 4 + 1 + 2 + 4 + 4 + 2;
    for(int i = 0; i < fc->Twalk.nwname; i++) {
        len += 2 + strlen(fc->Twalk.wname[i]);
    }
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Twalk;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->Twalk.fid, buffptr, &buffptr);
    toLittleE32(fc->Twalk.newfid, buffptr, &buffptr);
    toLittleE16(fc->Twalk.nwname, buffptr, &buffptr);
    for(int i = 0; i < fc->Twalk.nwname; i++) {
        buffptr = writeStr(fc->Twalk.wname[i], buffptr);
    }

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}
int write_Rwalk(int sockfd, FCall *fc) {
// size[4] Rwalk tag[2] nwqid[2] nwqid*(wqid[13])
    uint32_t len = 4 + 1 + 2 + 2 + (fc->Rwalk.nwqid * 13);
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rwalk;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE16(fc->Rwalk.nwqid, buffptr, &buffptr);
    for(size_t i = 0; i < fc->Rwalk.nwqid; i++) {
        writeQid(fc->Rwalk.wqid + i, buffptr, &buffptr);
    }

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}
int write_Topen(int sockfd, FCall *fc) {
// size[4] Twrite tag[2] fid[4] mode[1]
    uint32_t len = 4 + 1 + 2 + 4 + 1;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Topen;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->Topen.fid, buffptr, &buffptr);
    *buffptr++ = fc->Topen.mode;

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}
int write_Ropen(int sockfd, FCall *fc) {
// size[4] Ropen tag[2] qid[13] iounit[4]
    uint32_t len = 4 + 1 + 2 + 13 + 4;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Ropen;
    toLittleE16(fc->tag, buffptr, &buffptr);
    writeQid(&fc->Ropen.qid, buffptr, &buffptr);
    toLittleE32(fc->Ropen.iounit, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}

int write_Tcreate(int sockfd, FCall *fc) {
    return 0;
}
int write_Rcreate(int sockfd, FCall *fc) {
// size[4] Rcreate tag[2] qid[13] iounit[4]
    uint32_t len = 4 + 1 + 2 + 13 + 4;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rcreate;
    toLittleE16(fc->tag, buffptr, &buffptr);

    writeQid(&fc->Rcreate.qid, buffptr, &buffptr);
    toLittleE32(fc->Rcreate.iounit, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}
int write_Tread(int sockfd, FCall *fc) {
// size[4] Twrite tag[2] fid[4] offset[8] count[4]
    uint32_t len = 4 + 1 + 2 + 4 + 8 + 4;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Tread;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->Tread.fid, buffptr, &buffptr);
    toLittleE64(fc->Tread.offset, buffptr, &buffptr);
    toLittleE32(fc->Tread.count, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        exit(1);
    }

    return 1;
}
int write_Rread(int sockfd, FCall *fc) {
// size[4] Rread tag[2] count[4] data[count]
    uint32_t len = 4 + 1 + 2 + 4 + fc->Rread.count;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rread;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->Rread.count, buffptr, &buffptr);
    buffptr = writeBytes(fc->Rread.count, fc->Rread.data, buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        exit(1);
    }

    return 1;
}
int write_Twrite(int sockfd, FCall *fc) {
    return 0;
}
int write_Rwrite(int sockfd, FCall *fc) {
// size[4] Rwrite tag[2] count[4]
    uint32_t len = 4 + 1 + 2 + 4;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;
    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rwrite;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->Rwrite.count, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        exit(1);
    }

    return 1;
}
int write_Tclunk(int sockfd, FCall *fc) {
// size[4] Tclunk tag[2] fid[4]
    uint32_t len = 4 + 1 + 2 + 4;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;
    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Tclunk;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->Tclunk.fid, buffptr, &buffptr);

    printf("Writing tclunk fid: %u\n", fc->Tclunk.fid);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}

int write_Rclunk(int sockfd, FCall *fc) {
// size[4] Rclunk tag[2]
    uint32_t len = 4 + 1 + 2;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rclunk;
    toLittleE16(fc->tag, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}

int write_Tremove(int sockfd, FCall *fc) {
    return 0;
}

int write_Rremove(int sockfd, FCall *fc) {
// size[4] Rwstat tag[2]
    uint32_t len = 4 + 1 + 2;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rremove;
    toLittleE16(fc->tag, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}

int write_Tstat(int sockfd, FCall *fc) {
// size[4] Twrite tag[2] fid[4]

    uint32_t len = 4 + 1 + 2 + 4;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Tstat;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE32(fc->Tstat.fid, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}

int write_Rstat(int sockfd, FCall *fc) {
// size[4] Rstat tag[2] stat[n]
    uint16_t statlen = stat_length(&fc->Rstat);
    uint32_t len = 4 + 1 + 2 + 2 + statlen;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rstat;
    toLittleE16(fc->tag, buffptr, &buffptr);
    toLittleE16(statlen, buffptr, &buffptr);

    toLittleE16(statlen - 2, buffptr, &buffptr);
    toLittleE16(fc->Rstat.type, buffptr, &buffptr);
    toLittleE32(fc->Rstat.dev, buffptr, &buffptr);

    writeQid(&fc->Rstat.qid, buffptr, &buffptr);

    toLittleE32(fc->Rstat.mode, buffptr, &buffptr);
    toLittleE32(fc->Rstat.atime, buffptr, &buffptr);
    toLittleE32(fc->Rstat.mtime, buffptr, &buffptr);
    toLittleE64(fc->Rstat.length, buffptr, &buffptr);
    buffptr = writeStr(fc->Rstat.name, buffptr);
    buffptr = writeStr(fc->Rstat.uid, buffptr);
    buffptr = writeStr(fc->Rstat.gid, buffptr);
    buffptr = writeStr(fc->Rstat.muid, buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}

int write_Twstat(int sockfd, FCall *fc) {
    return 0;
}

int write_Rwstat(int sockfd, FCall *fc) {

    // size[4] Rwstat tag[2]
    uint32_t len = 4 + 1 + 2;
    uint8_t buffer[len];
    uint8_t *buffptr = buffer;

    toLittleE32(len, buffptr, &buffptr);
    *buffptr++ = Rwstat;
    toLittleE16(fc->tag, buffptr, &buffptr);

    if(send(sockfd, buffer, len, 0) == -1) {
        perror("Failed to send.");
        return 0;
    }
    return 1;
}

void free_rstat(RStat *rs) {
    free(rs->name);
    free(rs->uid);
    free(rs->gid);
    free(rs->muid);
}

void copy_rstat(RStat *dst, RStat *src) {
    *dst = *src;
    uint16_t namel = strlen(src->name) + 1;
    uint16_t uidl = strlen(src->uid) + 1;
    uint16_t gidl = strlen(src->gid) + 1;
    uint16_t muidl = strlen(src->muid) + 1;
    dst->name = malloc(namel);
    dst->uid = malloc(uidl);
    dst->gid = malloc(gidl);
    dst->muid = malloc(muidl);
    strcpy(dst->name, src->name);
    strcpy(dst->uid, src->uid);
    strcpy(dst->gid, src->gid);
    strcpy(dst->muid, src->muid);
}

void free_call(FCall *fc) {
    if(!fc) return;

    switch(fc->type) {
    case Tversion:
    case Rversion:
        free(fc->TRversion.version);
        break;
    case Tauth:
        free(fc->Tauth.uname);
        free(fc->Tauth.aname);
        break;
    case Tattach:
        free(fc->Tattach.uname);
        free(fc->Tattach.aname);
        break;
    case Rerror:
        free(fc->Rerror.ename);
        break;
    case Rstat:
        free_rstat(&fc->Rstat);
        break;
    case Rread:
        free(fc->Rread.data);
        break;
    case Twalk:
        for(uint16_t i = 0; i < fc->Twalk.nwname; i++) {
            free(fc->Twalk.wname[i]);
        }
        break;
    case Tcreate:
        free(fc->Tcreate.name);
        break;
    case Twrite:
        free(fc->Twrite.data);
        break;
    default:
        break;
    }
    free(fc);
}
