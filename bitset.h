
typedef struct {
    size_t bytes;
    uint8_t *bits;
} bitset;

bitset bitset_create(size_t bitcount);
void bitset_destroy(bitset b);
int getbit(bitset b, size_t bit);
void setbit(bitset b, size_t bit, int val);
ssize_t bitset_flip_first_zero(bitset b);
