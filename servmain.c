#include <unistd.h>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include "connection.h"
#include "fcall.h"
#include "filesystem.h"
#include "server.h"

void ram_open(const request *req, p9file *file) {
    response r;
    respond(req, &r);
}

void ram_read(const request *req, p9file *file, size_t offset, size_t count) {
    response r;
    char *str = "Hello, World!\n";
    size_t len = strlen(str);
    if(offset >= len) {
        r.read_response.data = NULL;
        r.read_response.count = 0;
    }
    else {
        r.read_response.data = (uint8_t *)str + offset;
        size_t size = len - offset;
        r.read_response.count = size > count ? count : size;
    }
    printf("Responding with response %p (%zu)\n", r.read_response.data, r.read_response.count);
    respond(req, &r);
}

void ram_write(const request *req, p9file *file, uint8_t *buffer, size_t offset, size_t count) {
    response r;
    r.write_response.count = count;
    respond(req, &r);
}

void ram_create(const request *req, p9file *dir, char *fname) {
    response r;
    r.create_response.fname = fname;
    r.create_response.uid = "root";
    r.create_response.gid = "root";
    r.create_response.length = 0;
    r.create_response.mode = 0;
    respond(req, &r);
}

void ram_setup(p9server *srv, p9fs *fs, p9file *root) {
    add_file(srv, fs, root, "hello", "root", "root", 14);
    add_file(srv, fs, root, "hello2", "root", "root", 14);
    p9file *newdir = add_dir(srv, fs, root, "somedir", "root", "root");
    add_file(srv, fs, newdir, "hello3", "root", "root", 14);
}

int main(void) {
    uint32_t mode = 0;
    for(int i = 0; i < 9; i++) {
        mode |= (1<<i);
    }
    mode = mode ^ (1 << 1); // o-w

    p9server server;
    server.default_mode = mode;
    server.open = ram_open;
    server.read = ram_read;
    server.write = ram_write;
    server.create = ram_create;
    server.setup = ram_setup;
    serve(&server);
}
