#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "fcall.h"
#include "reader.h"

void toLittleE16(uint16_t x, uint8_t *buff, uint8_t **newbuff) {
    buff[0] = (uint8_t) x & 0xFF;
    buff[1] = (uint8_t) (x >> 8u) & 0xFF;
    if(newbuff) *newbuff = buff + 2;
}

uint16_t fromLittleE16(uint8_t *buff, uint8_t **newbuff) {
    uint16_t ret =
        (uint16_t)buff[0]
        | (uint16_t)((buff[1] << 8u) & 0xFF00);
    if(newbuff) *newbuff = buff + 2;
    return ret;
}

void toLittleE32(uint32_t x, uint8_t *buff, uint8_t **newbuff) {
    buff[0] = (uint8_t) (x >>  0u) & 0xFF;
    buff[1] = (uint8_t) (x >>  8u) & 0xFF;
    buff[2] = (uint8_t) (x >> 16u) & 0xFF;
    buff[3] = (uint8_t) (x >> 24u) & 0xFF;
    if(newbuff) *newbuff = buff + 4;
}

uint32_t fromLittleE32(uint8_t *buff, uint8_t **newbuff) {
    uint32_t ret =
        (uint32_t)buff[0]
        | (uint32_t)((buff[1] <<  8u) & 0x0000FF00)
        | (uint32_t)((buff[2] << 16u) & 0x00FF0000)
        | (uint32_t)((buff[3] << 24u) & 0xFF000000);
    if(newbuff) *newbuff = buff + 4;
    return ret;
}

void toLittleE64(uint64_t x, uint8_t *buff, uint8_t **newbuff) {
    buff[0] = (uint8_t) (x >>  0u) & 0xFF;
    buff[1] = (uint8_t) (x >>  8u) & 0xFF;
    buff[2] = (uint8_t) (x >> 16u) & 0xFF;
    buff[3] = (uint8_t) (x >> 24u) & 0xFF;

    buff[4] = (uint8_t) (x >> 32u) & 0xFF;
    buff[5] = (uint8_t) (x >> 40u) & 0xFF;
    buff[6] = (uint8_t) (x >> 48u) & 0xFF;
    buff[7] = (uint8_t) (x >> 56u) & 0xFF;
    if(newbuff) *newbuff = buff + 8;
}

uint64_t fromLittleE64(uint8_t *buff, uint8_t **newbuff) {
    uint64_t ret =
        (uint64_t)buff[0]
        | (uint64_t)((buff[1] <<  8u) & 0x000000000000FF00)
        | (uint64_t)((buff[2] << 16u) & 0x0000000000FF0000)
        | (uint64_t)((buff[3] << 24u) & 0x00000000FF000000)
        | (((uint64_t)buff[4] << 32u) & 0x000000FF00000000)
        | (((uint64_t)buff[5] << 40u) & 0x0000FF0000000000)
        | (((uint64_t)buff[6] << 48u) & 0x00FF000000000000)
        | (((uint64_t)buff[7] << 56u) & 0xFF00000000000000);
    if(newbuff) *newbuff = buff + 8;
    return ret;
}

uint8_t *writeStr(char *str, uint8_t *buff) {
    uint16_t strl = strlen(str);
    toLittleE16(strl, buff, &buff);

    while(*str != 0) {
        *buff++ = *str++;
    }

    return buff;
}

char *readStrAlloc(uint8_t *buff, uint8_t **newbuff) {
    uint16_t strl = fromLittleE16(buff, &buff);
    char *outstr = malloc(strl + 1);

    for(int i = 0; i < strl; i++) {
        outstr[i] = buff[i];
    }
    outstr[strl] = 0;

    if(newbuff) *newbuff = buff + strl;
    return outstr;
}

uint8_t *readStr(char *sbuff, size_t buffsize, uint8_t *buff) {
    uint16_t strl = fromLittleE16(buff, &buff);
    uint16_t readl = strl;

    if(strl >= buffsize) readl = buffsize - 1;
    for(int i = 0; i < readl; i++) {
        sbuff[i] = buff[i];
    }
    sbuff[readl] = 0;

    return buff + strl;
}

Qid readQid(uint8_t *buff, uint8_t **newbuff) {
    Qid qid; //= malloc(sizeof *qid);
    qid.type = *(buff++);
    qid.vers = fromLittleE32(buff, &buff);
    qid.uid = fromLittleE64(buff, &buff);
    if(newbuff) *newbuff = buff;
    return qid;
}

void writeQid(Qid *qid, uint8_t *buff, uint8_t **newbuff) {
    *buff++ = qid->type;
    toLittleE32(qid->vers, buff, &buff);
    toLittleE64(qid->uid, buff, &buff);
    if(newbuff) *newbuff = buff;
}

uint8_t *writeBytes(uint32_t count, uint8_t *data, uint8_t *buff) {
    memcpy(buff, data, count);
    return buff + count;
}

uint8_t *readBytesAlloc(uint32_t count, uint8_t *buff, uint8_t **newbuff) {
    uint8_t *bytes = malloc(count);
    memcpy(bytes, buff, count);
    if(newbuff) *newbuff = buff + count;
    return bytes;
}
