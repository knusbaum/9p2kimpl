#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "bitset.h"

bitset bitset_create(size_t bitcount) {
    bitset bs;
    size_t bytes = bitcount / 8;
    printf("Allocating %zu bytes.\n", bytes);
    bs.bytes = bytes;
    if(bitcount % 8) bytes++;
    bs.bits = malloc(bytes);
    memset(bs.bits, 1, bytes);
    return bs;
}

void bitset_destroy(bitset b) {
    free(b.bits);
}

int getbit(bitset b, size_t bit) {
    size_t byte = bit / 8;
    size_t bitpos = bit % 8;
    return b.bits[byte] & (1 << bitpos) ? 1 : 0;
}

void setbit(bitset b, size_t bit, int val) {
    size_t byte = bit / 8;
    size_t bitpos = bit % 8;
    if(val) {
        b.bits[byte] |= (1 << bitpos);
    }
    else {
        b.bits[byte] &= ~(1 << bitpos);
    }
}

ssize_t bitset_flip_first_zero(bitset b) {
    int byte = -1;
    for(size_t i = 0; i < b.bytes; i++) {
        if(b.bits[i] != 0xFF) {
            byte=i;
            break;
        }
    }
    if(byte >= 0) {
        for(int i = 0; i < 8; i++) {
            if((b.bits[byte] & (1 << i)) == 0) {
                size_t bit = (byte * 8) + i;
                setbit(b, bit, 1);
                return bit;
            }
        }
    }
    return -1;
}
