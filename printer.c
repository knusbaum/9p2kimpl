#include <inttypes.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "9types.h"
#include "fcall.h"
#include "printer.h"

static char *open_mode_str(uint8_t mode);

static void print_TRversion(FCall *fc);
static void print_Tauth(FCall *fc);
static void print_Rauth(FCall *fc);
static void print_Rerror(FCall *fc);
static void print_Tflush(FCall *fc);
static void print_Tattach(FCall *fc);
static void print_Rattach(FCall *fc);
static void print_Twalk(FCall *fc);
static void print_Rwalk(FCall *fc);
static void print_Topen(FCall *fc);
static void print_Ropen(FCall *fc);
static void print_Tcreate(FCall *fc);
static void print_Rcreate(FCall *fc);
static void print_Tread(FCall *fc);
static void print_Rread(FCall *fc);
static void print_Twrite(FCall *fc);
static void print_Rwrite(FCall *fc);
static void print_Tclunk(FCall *fc);
static void print_Tremove(FCall *fc);
static void print_Tstat(FCall *fc);
static void print_Rstat(FCall *fc);
static void print_Twstat(FCall *fc);

void print_fcall(FCall *fc) {
    if(!fc) {
        printf("[NULL]\n");
        return;
    }
    printf("[%s] [Tag: %u ", string_for_type(fc->type), fc->tag);

    switch(fc->type) {
    case Tversion:
    case Rversion:
        print_TRversion(fc);
        break;
    case Tauth:
        print_Tauth(fc);
        break;
    case Rauth:
        print_Rauth(fc);
        break;
    case Rerror:
        print_Rerror(fc);
        break;
    case Tflush:
        print_Tflush(fc);
        break;
    case Tattach:
        print_Tattach(fc);
        break;
    case Rattach:
        print_Rattach(fc);
        break;
    case Twalk:
        print_Twalk(fc);
        break;
    case Rwalk:
        print_Rwalk(fc);
        break;
    case Topen:
        print_Topen(fc);
        break;
    case Ropen:
        print_Ropen(fc);
        break;
    case Tcreate:
        print_Tcreate(fc);
        break;
    case Rcreate:
        print_Rcreate(fc);
        break;
    case Tread:
        print_Tread(fc);
        break;
    case Rread:
        print_Rread(fc);
        break;
    case Twrite:
        print_Twrite(fc);
        break;
    case Rwrite:
        print_Rwrite(fc);
        break;
    case Tclunk:
        print_Tclunk(fc);
        break;
    case Tremove:
        print_Tremove(fc);
        break;
    case Tstat:
        print_Tstat(fc);
        break;
    case Rstat:
        print_Rstat(fc);
        break;
    case Twstat:
        print_Twstat(fc);
        break;
    default:
        break;
    }

    printf("]\n");
}

static char *open_mode_str(uint8_t mode) {
    switch((mode & 0x0F)) {
    case oread:
        if( mode & 0x10 ) return "trunc oread";
        return "oread";
        break;
    case owrite:
        if( mode & 0x10 ) return "trunc owrite";
        return "owrite";
        break;
    case ordwr:
        if( mode & 0x10 ) return "trunc ordwr";
        return "ordwr";
        break;
    case oexec:
        if( mode & 0x10 ) return "trunc oexec";
        return "oexec";
        break;
    default:
        return "<BAD VALUE>";
        break;
    }
}

static void print_TRversion(FCall *fc) {
    printf("msize: %u Version: %s ", fc->TRversion.msize, fc->TRversion.version);
}
static void print_Tauth(FCall *fc) {
}
static void print_Rauth(FCall *fc) {
}
static void print_Rerror(FCall *fc) {
    printf("ename: %s ", fc->Rerror.ename);
}
static void print_Tflush(FCall *fc) {
}
static void print_Tattach(FCall *fc) {
    printf("fid: %u afid: %u uname: %s aname: %s ",
           fc->Tattach.fid, fc->Tattach.afid,
           fc->Tattach.uname, fc->Tattach.aname);
}
static void print_qid(Qid qid) {
    printf("qid: [type: %x, vers: %u, uid: %" PRIu64 "] ",
           qid.type, qid.vers, qid.uid);
}

static void print_Rattach(FCall *fc) {
//    printf("qid: [type: %x, vers: %u, uid: %lu] ",
//           fc->Rattach.qid.type, fc->Rattach.qid.vers, fc->Rattach.qid.uid);
    print_qid(fc->Rattach.qid);
}
static void print_Twalk(FCall *fc) {
    printf("fid: %u newfid: %u nwname: %u ", fc->Twalk.fid, fc->Twalk.newfid, fc->Twalk.nwname);
    printf("< ");
    for(int i = 0; i < fc->Twalk.nwname; i++) {
        printf("%s ", fc->Twalk.wname[i]);
    }
    printf("> ");
}
static void print_Rwalk(FCall *fc) {
    printf("nwqid: %u ", fc->Rwalk.nwqid);
}
static void print_Topen(FCall *fc) {
    printf("fid: %u, mode: %s ", fc->Topen.fid, open_mode_str(fc->Topen.mode));
}
static void print_Ropen(FCall *fc) {
    print_qid(fc->Ropen.qid);
    printf("iounit: %u ", fc->Ropen.iounit);
}
static void print_Tcreate(FCall *fc) {
    printf("fid: %u, name: %s, perm: %o, mode: %s ",
           fc->Tcreate.fid, fc->Tcreate.name, fc->Tcreate.perm, open_mode_str(fc->Tcreate.mode));
}
static void print_Rcreate(FCall *fc) {
    print_qid(fc->Rcreate.qid);
    printf("iounit: %u ", fc->Rcreate.iounit);
}
static void print_Tread(FCall *fc) {
    printf("fid: %u, offset: %" PRIu64 ", count: %u ",
           fc->Tread.fid, fc->Tread.offset, fc->Tread.count);
}
static void print_Rread(FCall *fc) {
    printf("count: %u ", fc->Rread.count);
}
static void print_Twrite(FCall *fc) {
    printf("fid: %u, offset: %" PRIu64 ", count: %u ",
           fc->Twrite.fid, fc->Twrite.offset, fc->Twrite.count);
}
static void print_Rwrite(FCall *fc) {
    printf("count: %u ", fc->Rwrite.count);
}
static void print_Tclunk(FCall *fc) {
    printf("fid: %u ", fc->Tclunk.fid);
}
static void print_Tremove(FCall *fc) {
}
static void print_Tstat(FCall *fc) {
    printf("fid: %u ", fc->Tstat.fid);
}

static void print_stat(RStat *rs) {
    printf("type: %u, dev: %u ",
           rs->type, rs->dev);
    print_qid(rs->qid);
    printf("mode: %o, length: %" PRIu64 ", name: %s, uid: %s, gid: %s, muid: %s ",
           rs->mode, rs->length, rs->name, rs->uid, rs->gid, rs->muid);
}

static void print_Rstat(FCall *fc) {
    print_stat(&fc->Rstat);
}
static void print_Twstat(FCall *fc) {
    printf("fid: %u ", fc->Twstat.fid);
    print_stat(&fc->Twstat.stat);
}

static void mode_string(uint32_t mode, char *modebuff) {
    int dir = mode & (1 << 31);

    int ur = mode & (1 << 8);
    int uw = mode & (1 << 7);
    int ux = mode & (1 << 6);
    int gr = mode & (1 << 5);
    int gw = mode & (1 << 4);
    int gx = mode & (1 << 3);
    int or = mode & (1 << 2);
    int ow = mode & (1 << 1);
    int ox = mode & (1 << 0);

    modebuff[0] = dir ? 'd' : '-';
    modebuff[1] =  ur ? 'r' : '-';
    modebuff[2] =  uw ? 'w' : '-';
    modebuff[3] =  ux ? 'x' : '-';
    modebuff[4] =  gr ? 'r' : '-';
    modebuff[5] =  gw ? 'w' : '-';
    modebuff[6] =  gx ? 'x' : '-';
    modebuff[7] =  or ? 'r' : '-';
    modebuff[8] =  ow ? 'w' : '-';
    modebuff[9] =  ox ? 'x' : '-';
}

void print_directory(RStats rs) {
    size_t user_len = 0;
    size_t group_len = 0;
    size_t len_len = 0;
    for(ssize_t i = 0; i < rs.stat_count; i++) {
        RStat *stat = rs.stats + i;

        ssize_t len = strlen(stat->uid);
        user_len = len > user_len ? len : user_len;

        len = strlen(stat->gid);
        group_len = len > group_len ? len : group_len;

        char buffer[1024];
        snprintf(buffer, 1024, "%" PRIu64, stat->length);
        len = strlen(buffer);
        len_len = len > len_len ? len : len_len;
    }

    char fmtbuffer[1024];
    snprintf(fmtbuffer, 1024, "%%s %%-%zds %%-%zds %%%zdd %%s %%s\n",
             user_len, group_len, len_len);
    for(ssize_t i = 0; i < rs.stat_count; i++) {
        RStat *stat = rs.stats + i;
        char modebuff[11];
        modebuff[10] = 0;
        mode_string(stat->mode, modebuff);

        time_t mtime = stat->mtime;
        char timebuff[200];
        struct tm lt;
        memset(&lt, 0, sizeof lt);
        localtime_r(&mtime, &lt);
        strftime(timebuff, 200, "%b %d %H:%M", &lt);

        printf(fmtbuffer,
               modebuff, stat->uid, stat->gid, stat->length,
               timebuff, stat->name);
    }
}
