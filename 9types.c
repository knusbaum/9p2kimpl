#include <stdint.h>
#include "9types.h"

char *types[] = {
    "Tversion",
    "Rversion",
    "Tauth",
    "Rauth",
    "Tattach",
    "Rattach",
    "Terror",
    "Rerror",
    "Tflush",
    "Rflush",
    "Twalk",
    "Rwalk",
    "Topen",
    "Ropen",
    "Tcreate",
    "Rcreate",
    "Tread",
    "Rread",
    "Twrite",
    "Rwrite",
    "Tclunk",
    "Rclunk",
    "Tremove",
    "Rremove",
    "Tstat",
    "Rstat",
    "Twstat",
    "Rwstat",
    "Tmax"
};

char * string_for_type(uint8_t type) {
    return types[type-100];
}
