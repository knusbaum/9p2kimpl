
typedef struct p9conn p9conn;

p9conn *create_p9conn(int fd);
void destroy_p9conn(p9conn *conn);

int get_conn_fd(p9conn *conn);
void set_root_fid(p9conn *conn, uint32_t fid);
const char *get_fid_path(p9conn *conn, uint32_t fid);
void set_fid_path(p9conn *conn, uint32_t fid, char *path);
uint8_t get_fid_openmode(p9conn *conn, uint32_t fid);
void set_fid_openmode(p9conn *conn, uint32_t fid, uint8_t mode);
uint64_t get_fid_openoffset(p9conn *conn, uint32_t fid);
void set_fid_openoffset(p9conn *conn, uint32_t fid, uint64_t offset);
void free_fid(p9conn *conn, uint32_t fid);

/* Connection Storage */
typedef struct conn_list conn_list;
conn_list *create_conn_list();
void add_conn(conn_list *conn_l, p9conn *c);
p9conn *rm_conn_by_fd(conn_list *conn_l, int fd);
size_t conn_count(conn_list *conn_l);
p9conn *nth_conn(conn_list *conn_l, size_t nth);
int highest_conn_fd(conn_list *conn_l);
