#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "fcall.h"
#include "bitset.h"
#include "9client.h"
#include "9types.h"
#include "9net.h"
#include "printer.h"


static FCall *handle_reply(int sockfd, uint8_t type) {
    FCall *rfc = parse_call(sockfd);

    if(rfc) {
        printf(">>>\t");
        print_fcall(rfc);
        if(rfc->type == Rerror) {
            fprintf(stderr, "Server returned error: \"%s\"\n", rfc->Rerror.ename);
            free_call(rfc);
            return NULL;
        }
        else if (rfc->type != type) {
            fprintf(stderr, "Failed read proper response from server.\n");
            free_call(rfc);
            return NULL;
        }
    }
    else {
        fprintf(stderr, "Failed read from server.\n");
        return NULL;
    }

    return rfc;
}

p9client *p9_connect(char *host, char *port, char *uname, char *aname) {
    int sockfd = p9net_connect_to(host, port);
    if(sockfd == -1) {
        fprintf(stderr, "Failed to connect to %s:%s\n", host, port);
        return NULL;
    }

    FCall fc;
    fc.type = Tversion;
    fc.tag = 0;
    fc.TRversion.msize = MAXBUFF;
    fc.TRversion.version = "9P2000";
    printf("<<<\t");
    print_fcall(&fc);
    if(!write_fcall(sockfd, &fc)) {
        fprintf(stderr, "Failed to send version to %s:%s\n", host, port);
        return NULL;
    }

    p9client *p9 = malloc(sizeof *p9);
    p9->socket = sockfd;
    p9->maxbuff = 0;
    // These are waaay too big. Need to build lazy allocation.
    p9->tagset = bitset_create((uint16_t)~0);
    p9->fidset = bitset_create((uint32_t)~0);
    p9->rootfid = 0;

    FCall *rfc;
    if((rfc = handle_reply(sockfd, Rversion))) {
        p9->maxbuff = rfc->TRversion.msize;
        free_call(rfc);
    }
    else {
        free(p9);
        return NULL;
    }
    printf("------------------------------------\n");

    fc.type = Tattach;
    fc.tag = 0;
    fc.Tattach.fid = 0;
    fc.Tattach.afid = -1;
    fc.Tattach.uname = uname;
    fc.Tattach.aname = aname;
    printf("<<<\t");
    print_fcall(&fc);
    if(!write_fcall(sockfd, &fc)) {
        fprintf(stderr, "Failed to attach to %s:%s\n", host, port);
        return NULL;
    }

    if((rfc = handle_reply(sockfd, Rattach))) {
        free_call(rfc);
    }
    else {
        free(p9);
        return NULL;
    }
    printf("------------------------------------\n");

    fc.type = Tstat;
    fc.tag = 0;
    fc.Tstat.fid = 0;
    printf("<<<\t");
    print_fcall(&fc);
    if(!write_fcall(sockfd, &fc)) {
        fprintf(stderr, "Failed to stat root on %s:%s\n", host, port);
        return NULL;
    }

    if((rfc = handle_reply(sockfd, Rstat))) {
        p9->rootstat = rfc->Rstat;
        copy_rstat(&p9->rootstat, &rfc->Rstat);
        free_call(rfc);
    }
    else {
        free(p9);
        return NULL;
    }
    printf("------------------------------------\n");
    return p9;
}

void p9_shutdown(p9client *p9) {
    shutdown(p9->socket, SHUT_RDWR);
    close(p9->socket);
    free_rstat(&p9->rootstat);
    bitset_destroy(p9->tagset);
    bitset_destroy(p9->fidset);
    free(p9);
}

p9fhandle p9_get_root(p9client *p9) {
    p9fhandle h;
    h.handle = p9->rootfid;
    h.position = 0;
    return h;
}

static FCall *write_twalk(uint16_t tag, uint32_t fid, uint32_t newfid, uint16_t nwname, char** wnames, int sockfd) {
    FCall fc;
    fc.type = Twalk;
    fc.tag = tag;
    fc.Twalk.fid = fid;
    fc.Twalk.newfid = newfid;
    fc.Twalk.nwname = nwname;
    for(int i = 0; i < fc.Twalk.nwname; i++) {
        fc.Twalk.wname[i] = wnames[i];
    }
    printf("<<<\t");
    print_fcall(&fc);
    if(!write_fcall(sockfd, &fc)) {
        fprintf(stderr, "Failed to send walk request.\n");
        return NULL;
    }

    return handle_reply(sockfd, Rwalk);
}

p9fhandle p9_duplicate(p9client *p9, p9fhandle h) {
    uint16_t newtag = bitset_flip_first_zero(p9->tagset);
    uint32_t newfh = bitset_flip_first_zero(p9->fidset);
    FCall *rfc =
        write_twalk(newtag, h.handle, newfh,
                    0, NULL, p9->socket);
    if(!rfc) {
        setbit(p9->fidset, newfh, 0);
        h.handle = -1;
        return h;
    }
    h.handle = newfh;
    free_call(rfc);
    setbit(p9->tagset, newtag, 0);
    return h;
}

int p9_free_handle(p9client *p9, p9fhandle h) {
    uint16_t newtag = bitset_flip_first_zero(p9->tagset);
    FCall fc;
    fc.type = Tclunk;
    fc.tag = newtag;
    fc.Tclunk.fid = h.handle;
    printf("<<<\t");
    print_fcall(&fc);
    if(!write_fcall(p9->socket, &fc)) {
        fprintf(stderr, "Failed to send clunk request.\n");
        setbit(p9->tagset, newtag, 0);
        return 0;
    }

    FCall *rfc = handle_reply(p9->socket, Rclunk);
    if(!rfc) {
        fprintf(stderr, "Failed to get clunk response from server.\n");
        setbit(p9->tagset, newtag, 0);
        return 0;
    }
    free_call(rfc);
    setbit(p9->tagset, newtag, 0);
    setbit(p9->fidset, h.handle, 0);
    return 1;
}

// This limits the depth that one can traverse with a single
// call to p9_chdir to 256. This might be practically unreachable.
#define MAXDEPTH 256

int p9_chdir(p9client *p9, p9fhandle h, char *dir) {
    char *context = NULL;
    char dirbuff[strlen(dir) + 1];
    strcpy(dirbuff, dir);

    int i = 0;
    char *dirsplit[MAXDEPTH];
    char *currtok = strtok_r(dirbuff, "/", &context);
    do {
        dirsplit[i++] = currtok;
    } while(i < 256 && (currtok = strtok_r(NULL, "/", &context)) != NULL);

    uint16_t newtag = bitset_flip_first_zero(p9->tagset);
    FCall *rfc =
        write_twalk(newtag, h.handle, h.handle,
                    i, dirsplit, p9->socket);
    if(!rfc) {
        return 0;
    }

    free_call(rfc);
    setbit(p9->tagset, newtag, 0);
    return 1;
}

int p9_open(p9client *p9, p9fhandle h, uint8_t mode) {
    FCall fc;
    fc.type = Topen;
    fc.tag = bitset_flip_first_zero(p9->tagset);
    fc.Topen.fid = h.handle;
    fc.Topen.mode = mode;
    printf("<<<\t");
    print_fcall(&fc);
    if(!write_fcall(p9->socket, &fc)) {
        printf("Failed to send open request.\n");
        setbit(p9->tagset, fc.tag, 0);
        return 0;
    }

    FCall *rfc = handle_reply(p9->socket, Ropen);

    if(!rfc) {
        printf("Failed to get open response from server.\n");
        setbit(p9->tagset, fc.tag, 0);
        return 0;
    }
    free_call(rfc);
    setbit(p9->tagset, fc.tag, 0);
    return 1;
}

uint32_t p9_read(p9client *p9, p9fhandle *h, uint8_t *buff, uint32_t count) {
    uint32_t start = h->position;
    uint32_t readcount = 0;
    uint16_t tag = bitset_flip_first_zero(p9->tagset);
    uint32_t maxread = p9->maxbuff - 30;

    while(readcount < count) {
        uint32_t readamount =
            count - readcount > maxread ? maxread : count - readcount;

        FCall fc;
        fc.type = Tread;
        fc.tag = tag;
        fc.Tread.fid = h->handle;
        fc.Tread.offset = start + readcount;
        fc.Tread.count = readamount;
        printf("<<<\t");
        print_fcall(&fc);
        if(!write_fcall(p9->socket, &fc)) {
            fprintf(stderr, "Failed to send read request.\n");
            setbit(p9->tagset, tag, 0);
            return 0;
        }
        FCall *rfc = handle_reply(p9->socket, Rread);
        if(!rfc) {
            fprintf(stderr, "Failed to get read response from server.\n");
            setbit(p9->tagset, tag, 0);
            return 0;
        }

        if(rfc->Rread.count == 0) {
            free_call(rfc);
            break;
        }

        memcpy(buff + readcount, rfc->Rread.data, rfc->Rread.count);
        readcount += rfc->Rread.count;
        free_call(rfc);
    }
    h->position += readcount;
    setbit(p9->tagset, tag, 0);
    return readcount;
}

RStat *p9_stat_file(p9client *p9, p9fhandle h) {
    FCall fc;
    fc.type = Tstat;
    fc.tag = bitset_flip_first_zero(p9->tagset);
    fc.Tstat.fid = h.handle;
    printf("<<<\t");
    print_fcall(&fc);
    if(!write_fcall(p9->socket, &fc)) {
        fprintf(stderr, "Failed to send stat request.\n");
        setbit(p9->tagset, fc.tag, 0);
        return NULL;
    }


    FCall *rfc = handle_reply(p9->socket, Rstat);
    if(!rfc) {
        fprintf(stderr, "Failed to get stat response from server.\n");
        setbit(p9->tagset, fc.tag, 0);
        return NULL;
    }
    setbit(p9->tagset, fc.tag, 0);
    RStat *rs = malloc(sizeof *rs);
    copy_rstat(rs, &rfc->Rstat);
    free_call(rfc);
    return rs;
}

void p9_free_stat(RStat *rs) {
    free_rstat(rs);
    free(rs);
}
