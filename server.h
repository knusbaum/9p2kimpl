typedef struct request request;

typedef struct p9server {
    uint32_t default_mode;
    void (*open)(const request *req, p9file *file);
    void (*read)(const request *req, p9file *file, size_t offset, size_t count);
    void (*write)(const request *req, p9file *file, uint8_t *buffer, size_t offset, size_t count);
    void (*create)(const request *req, p9file *dir, char *fname);
    void (*setup)(struct p9server *srv, p9fs *fs, p9file *root);
} p9server;


typedef struct response {
    union {
        struct read_response {
            uint8_t *data;
            size_t count;
        } read_response;
        struct write_response {
            size_t count;
        } write_response;
        struct create_response {
            char *fname;
            //RStat *stat;
            char *uid;
            char *gid;
            size_t length;
            uint32_t mode;
        } create_response;
    };
} response;

int serve(p9server *p9srv);
p9file *add_file(p9server *p9srv, p9fs *fs, p9file *parent, char *fname, char *uid, char * gid, size_t length);
p9file *add_dir(p9server *p9srv, p9fs *fs, p9file *parent, char *dname, char *uid, char * gid);
void respond(const request *req, response *r);
