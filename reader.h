#ifndef READER_H
#define READER_H

void toLittleE16(uint16_t x, uint8_t *buff, uint8_t **newbuff);
uint16_t fromLittleE16(uint8_t *buff, uint8_t **newbuff);
void toLittleE32(uint32_t x, uint8_t *buff, uint8_t **newbuff);
uint32_t fromLittleE32(uint8_t *buff, uint8_t **newbuff);
void toLittleE64(uint64_t x, uint8_t *buff, uint8_t **newbuff);
uint64_t fromLittleE64(uint8_t *buff, uint8_t **newbuff);

uint8_t *writeStr(char *str, uint8_t *buff);
char *readStrAlloc(uint8_t *buff, uint8_t **newbuff);
uint8_t *readStr(char *sbuff, size_t buffsize, uint8_t *buff);

Qid readQid(uint8_t *buff, uint8_t **newbuff);
void writeQid(Qid *qid, uint8_t *buff, uint8_t **newbuff);

uint8_t *writeBytes(uint32_t count, uint8_t *data, uint8_t *buff);
uint8_t *readBytesAlloc(uint32_t count, uint8_t *buff, uint8_t **newbuff);

#endif
