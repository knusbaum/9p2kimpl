#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "fcall.h"
#include "9types.h"
#include "connection.h"
#include "filesystem.h"

typedef struct p9file_elem {
    p9file file;
    struct p9file_elem *prev;
    struct p9file_elem *next;
} p9file_elem;

struct p9fs {
    p9file_elem *head;
    p9file_elem *tail;
    char *uname;
    uint32_t curruid;
};

void dump_files(p9fs *fs) {
    p9file_elem *current = fs->head;
    while(current) {
        printf("name: %s\n", current->file.name);
        current = current->next;
    }
}

p9fs *create_fs() {
    p9fs *fs = malloc(sizeof (p9fs));
    if(!fs) return NULL;
    memset(fs, 0, sizeof *fs);

    return fs;
}

void destroy_fs(p9fs *fs) {
    p9file_elem *current = fs->head;
    while(current) {
        free(current->file.name);
        free(current->file.contents);
        if(current->file.subfile_n) {
            free(current->file.subfiles);
        }
        current = current->next;
    }
    free(fs->uname);
    free(fs);
}

void set_fs_user(p9fs *fs, char *name) {
    fs->uname = malloc(strlen(name) + 1);
    strcpy(fs->uname, name);
}

const char *get_fs_user(p9fs *fs) {
    return fs->uname;
}

p9file *fs_get_file(p9fs *fs, const char *fname) {
    if(fname == NULL) return NULL;

    p9file_elem *current = fs->head;
    while(current) {
        if(strcmp(current->file.name, fname) == 0) {
            return &current->file;
        }
        current = current->next;
    }
    return NULL;
}

p9file *fs_add_file(p9fs *fs, char *fname, uint8_t *contents, RStat *rstat) {
    p9file_elem *elem = malloc(sizeof (p9file_elem));
    p9file *file = &elem->file;

    if(fs->head == NULL) {
        fs->head = elem;
        fs->tail = elem;
        elem->prev = NULL;
    }
    else {
        fs->tail->next = elem;
        elem->prev = fs->tail;
        fs->tail = elem;
    }
    elem->next = NULL;

    file->name = fname;
    file->contents = contents;
    copy_rstat(&file->rstat, rstat);
    file->subfile_n = 0;
    file->subfiles = NULL;
    return file;
}

void fs_add_subfile(p9fs *fs, p9file *dir, p9file *f) {
    printf("Adding subfile %s to directory %s\n", f->name, dir->name);
    if(dir->subfiles){
        dir->subfiles = realloc(dir->subfiles,
                                (dir->subfile_n + 1) * sizeof (p9file *));
    }
    else {
        dir->subfiles = malloc(sizeof (p9file *));
    }
    dir->subfiles[dir->subfile_n] = f;
    dir->subfile_n++;
    f->parent = dir;
}

static void fs_rm_subfile(p9fs *fs, p9file *dir, p9file *f) {

    for(size_t i = 0; i < dir->subfile_n; i++) {
        if(dir->subfiles[i] == f) {
            memmove(&dir->subfiles[i], &dir->subfiles[i + 1], (dir->subfile_n - i - 1) * sizeof (p9file *));
            dir->subfiles = realloc(dir->subfiles,
                                    (dir->subfile_n - 1) * sizeof (p9file *));
            dir->subfile_n--;
        }
    }
}

void fs_write_dir_rstats(p9fs *fs, p9file *dir) {
    size_t total_length = 0;
    for(size_t i = 0; i < dir->subfile_n; i++) {
        total_length += stat_length(&dir->subfiles[i]->rstat);
    }

    if(dir->contents) {
        dir->contents = realloc(dir->contents, total_length);
    }
    else {
        dir->contents = malloc(total_length);
    }
    dir->rstat.length = total_length;

    uint8_t *dirbuff = dir->contents;
    for(size_t i = 0; i < dir->subfile_n; i++) {
        write_rstat(&dir->subfiles[i]->rstat, dirbuff, &dirbuff);
    }
}

p9file *file_for_fid(p9fs *fs, p9conn *conn, uint32_t fid) {
    return fs_get_file(fs, get_fid_path(conn, fid));
}

p9file *open_file(p9fs *fs, p9conn *conn, uint32_t fid, uint8_t openmode) {
    p9file *file = file_for_fid(fs, conn, fid);
    if(!file) {
        return NULL;
    }
    if(get_fid_openmode(conn, fid) != none) {
        // File must already be open (?)
        return NULL;
    }

    set_fid_openmode(conn, fid, openmode);
    set_fid_openoffset(conn, fid, file->rstat.length);

    return file;
}

Qid alloc_qid(p9fs *fs, uint8_t type) {
    uint32_t uid = ++(fs->curruid);
    Qid q;
    q.type = type;
    q.vers = 0;
    q.uid = uid;
    return q;
}

static void free_file(p9fs *fs, p9conn *conn, p9file_elem *elem) {
    printf("Deleting file with %zu subfiles.\n", elem->file.subfile_n);

    for(size_t i = 0; i < elem->file.subfile_n; i++) {
        p9file_elem *subfile = (p9file_elem *)elem->file.subfiles[i];
        free_file(fs, conn, subfile);
    }

    free(elem->file.name);
    free(elem->file.contents);
    free_rstat(&elem->file.rstat);
    if(elem->file.subfile_n) {
        free(elem->file.subfiles);
    }
    fs_rm_subfile(fs, elem->file.parent, &elem->file);
    // Try to free fids or not?
    //free_fid(conn, fid);
    if(fs->head == elem) {
        fs->head = elem->next;
    }
    if(fs->tail == elem) {
        fs->tail = elem->prev;
    }

    if(elem->prev) {
        elem->prev->next = elem->next;
    }
    if(elem->next) {
        elem->next->prev = elem->prev;
    }
    free(elem);
}


void delfile(p9fs *fs, p9conn *conn, uint32_t fid) {
    // C allows us to cast this.
    p9file_elem *elem = (p9file_elem *)file_for_fid(fs, conn, fid);
    if(!elem) {
        return;
    }

// Not sure we need this anymore. It's supposed to handle files that are deleted
// while opened by another fid, but I think it works okay now.
// Clients with fid's open to the deleted file get Rerror back.
    // Need to rebuild this at some point.
//    // This is an atrocity
//    for(size_t i = 0; i < FILECOUNT; i++) {
//        const char *fid_path = get_fid_path(conn, i);
//        //printf("[%s]:[%s]\n", fid_path, file->name);
//        if(fid_path
//           && strcmp(fid_path, file->name) == 0
//           && get_fid_openmode(conn, i) != none) {
//            // We need to do some special stuff here.
//            // Until then, blow up.
//            fprintf(stderr, "Tried to delete a file which is open.\n");
//            exit(1);
//        }
//    }

    free_file(fs, conn, elem);
    free_fid(conn, fid);
}
