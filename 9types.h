#ifndef _9TYPES_H
#define _9TYPES_H

enum
{
    Tversion =100,
    Rversion,
    Tauth =102,
    Rauth,
    Tattach =104,
    Rattach,
    Terror =106,/* illegal */
    Rerror,
    Tflush =108,
    Rflush,
    Twalk =110,
    Rwalk,
    Topen =112,
    Ropen,
    Tcreate =114,
    Rcreate,
    Tread =116,
    Rread,
    Twrite =118,
    Rwrite,
    Tclunk =120,
    Rclunk,
    Tremove =122,
    Rremove,
    Tstat =124,
    Rstat,
    Twstat =126,
    Rwstat,
    Tmax,
};

enum omode {
    oread = 0,
    owrite = 1,
    ordwr = 2,
    oexec = 3,
    none = 4
};

char * string_for_type(uint8_t type);

#endif
