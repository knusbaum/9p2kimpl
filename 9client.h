#define MAXBUFF 8192

// 

typedef struct {
    int socket;
    uint16_t maxbuff;
    char buffer[MAXBUFF];
    bitset tagset;
    bitset fidset;
    uint32_t rootfid;
    RStat rootstat;
} p9client;

typedef struct {
    uint32_t handle;
    uint64_t position;
} p9fhandle;

p9client *p9_connect(char *host, char *port, char *uname, char *aname);
void p9_shutdown(p9client *p9);
p9fhandle p9_get_root(p9client *p9);
p9fhandle p9_duplicate(p9client *p9, p9fhandle h);
int p9_free_handle(p9client *p9, p9fhandle h);
int p9_chdir(p9client *p9, p9fhandle h, char *dir);
int p9_open(p9client *p9, p9fhandle h, uint8_t mode);
uint32_t p9_read(p9client *p9, p9fhandle *h, uint8_t *buff, uint32_t count);
RStat *p9_stat_file(p9client *p9, p9fhandle h);
void p9_free_stat(RStat *);
