#ifndef _9INET_H
#define _9INET_H

int p9net_connect_to(char *host, char *port);
int p9net_listen_on(char *host, char *port);

#endif
