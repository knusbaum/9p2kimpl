#define IOUNIT 8168

typedef struct _p9file {
    uint8_t valid;
    char *name;
    uint8_t *contents;
    RStat rstat;
    size_t subfile_n;
    struct _p9file **subfiles;
    struct _p9file *parent;
} p9file;

typedef struct p9fs p9fs;

p9file *fs_get_file(p9fs *fs, const char *fname);
p9file *fs_add_file(p9fs *fs, char *fname, uint8_t *contents, RStat *rstat);
void fs_add_subfile(p9fs *fs, p9file *dir, p9file *f);
//void fs_rm_subfile(p9fs *fs, p9file *dir, p9file *f);
void fs_write_dir_rstats(p9fs *fs, p9file *dir);


/* Other stuff (probably need to refactor API)*/
void dump_files(p9fs *fs);
p9fs *create_fs();
void destroy_fs(p9fs *fs);
void set_fs_user(p9fs *fs, char *name); // Should be per connection, but is not currently.
const char *get_fs_user(p9fs *fs);
p9file *file_for_fid(p9fs *fs, p9conn *conn, uint32_t fid);
p9file *open_file(p9fs *fs, p9conn *conn, uint32_t fid, uint8_t openmode);
Qid alloc_qid(p9fs *fs, uint8_t type);
void delfile(p9fs *fs, p9conn *conn, uint32_t fid);
