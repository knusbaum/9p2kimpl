#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include "9net.h"

static struct addrinfo *getAddrInfoFor(char *host, char *port) {
    struct addrinfo hints;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    struct addrinfo *addrp;
    getaddrinfo(host,
                port,
                &hints,
                &addrp);

    return addrp;
}

int p9net_connect_to(char *host, char *port) {
    struct addrinfo *addrp = getAddrInfoFor(host, port);
    if(!addrp) return -1;

    int sockfd = socket(addrp->ai_family, addrp->ai_socktype, addrp->ai_protocol);
    if(connect(sockfd, addrp->ai_addr, addrp->ai_addrlen) == -1) {
        printf("Failed to connect.\n");
        exit(1);
    }

    freeaddrinfo(addrp);
    return sockfd;
}

int p9net_listen_on(char *host, char *port) {
    struct addrinfo *addrp = getAddrInfoFor(host, port);
    if(!addrp) return -1;

    int sockfd = socket(addrp->ai_family, addrp->ai_socktype, addrp->ai_protocol);
    printf("Got socket: %d\n", sockfd);

    int yes = 1;
    if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1 )
    {
        perror("setsockopt");
    }

    if(bind(sockfd, addrp->ai_addr, addrp->ai_addrlen)) {
        printf("Failed to bind to socket.\n");
        exit(1);
    }
    if(listen(sockfd, 10)) {
        printf("Failed to listen on socket.\n");
        exit(1);
    }
    freeaddrinfo(addrp);
    return sockfd;
}
