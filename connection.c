#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include "9types.h"
#include "connection.h"

typedef struct fid_info {
    char *name;
    uint32_t fid;
    enum omode openmode;
    uint64_t openoffset;
} fid_info;

typedef struct {
    size_t infocount;
    fid_info *first;
} info_sentry;

#define MODSIZE 10000
#define STARTSIZE 1

struct p9conn {
    int fd;
    info_sentry fid[MODSIZE];
};

fid_info *info_for_fid(p9conn *conn, uint32_t fid) {
    // If the sentry at fid % MODSIZE is empty, allocate some
    // fid_info structs and initialize them.
    if(conn->fid[fid % MODSIZE].infocount == 0) {
        conn->fid[fid % MODSIZE].first = malloc(STARTSIZE * sizeof (fid_info));
        conn->fid[fid % MODSIZE].infocount = STARTSIZE;
        fid_info *current = conn->fid[fid % MODSIZE].first;
        for(size_t i = 0; i < STARTSIZE; i++) {
            current[i].name = NULL;
            current[i].fid = ~0;
            current[i].openmode = none;
            current[i].openoffset = 0;
        }
    }

    // Look through available info structs.
    info_sentry *head = &conn->fid[fid % MODSIZE];
    fid_info *current = head->first;
    for(size_t i = 0; i < head->infocount; i++) {
        if(current->name == NULL) {
            // we've found the end of the allocated
            // infos. Return this empty one.
            current->fid = fid;
            return current;
        }
        else if(current->fid == fid) {
            return current;
        }
        current++;
    }


    // Didn't find our FID in the list and the list is full.
    // Need to allocate more space for info structs.
    // The struct to be returned is the first one of the newly
    // allocated array, that is, that at index head->infocount
    size_t return_offset = head->infocount;
    head->infocount *= 2;
    head->first = realloc(head->first, head->infocount * sizeof (fid_info));
    current = head->first;
    for(size_t i = head->infocount / 2; i < head->infocount; i++) {
        current[i].name = NULL;
        current[i].fid = ~0;
        current[i].openmode = none;
        current[i].openoffset = 0;
    }

    head->first[return_offset].fid = fid;
    return &head->first[return_offset];
}

p9conn *create_p9conn(int fd) {
    p9conn *conn = malloc(sizeof (p9conn));
    if(!conn) return NULL;

    memset(conn, 0, sizeof (p9conn));
    conn->fd = fd;
    return conn;
}

void destroy_p9conn(p9conn *conn) {
    for(size_t i = 0; i < MODSIZE; i++) {
        fid_info *infos = conn->fid[i].first;
        for(size_t j = 0; j < conn->fid[i].infocount; j++) {
            free(infos[j].name);
        }
        free(infos);
    }
    free(conn);
}

int get_conn_fd(p9conn *conn) {
    return conn->fd;
}

void set_root_fid(p9conn *conn, uint32_t fid) {
    char *rootname = malloc(2);
    strcpy(rootname, "/");

    fid_info *info = info_for_fid(conn, fid);
    info->name = rootname;
    info->openmode = none;
    info->openoffset = 0;
}

const char *get_fid_path(p9conn *conn, uint32_t fid) {
    fid_info *info = info_for_fid(conn, fid);
    return info->name;
}

void set_fid_path(p9conn *conn, uint32_t fid, char *path) {
    fid_info *info = info_for_fid(conn, fid);
    if(info->name) {
        free(info->name);
    }
    info->name = path;
    info->openmode = none;
    info->openoffset = 0;
}

uint8_t get_fid_openmode(p9conn *conn, uint32_t fid) {
    fid_info *info = info_for_fid(conn, fid);
    return info->openmode;
}

void set_fid_openmode(p9conn *conn, uint32_t fid, uint8_t mode) {
    fid_info *info = info_for_fid(conn, fid);
    info->openmode = mode;
}

uint64_t get_fid_openoffset(p9conn *conn, uint32_t fid) {
    fid_info *info = info_for_fid(conn, fid);
    return info->openoffset;
}

void set_fid_openoffset(p9conn *conn, uint32_t fid, uint64_t offset) {
    fid_info *info = info_for_fid(conn, fid);
    info->openoffset = offset;
}

void free_fid(p9conn *conn, uint32_t fid) {
    fid_info *info = info_for_fid(conn, fid);
    if(info->name) {
        free(info->name);
        info->name = NULL;
    }

    info->openmode = none;
    info->openoffset = 0;
}

/* For the server loop */
struct conn_elem {
    p9conn *conn;
    struct conn_elem *next;
};

struct conn_list {
    struct conn_elem *head;
    struct conn_elem *tail;
    size_t nconns;
};

conn_list *create_conn_list() {
    conn_list *list = malloc(sizeof (conn_list));
    list->head = NULL;
    list->tail = NULL;
    list->nconns = 0;
    return list;
}

void add_conn(conn_list *conn_l, p9conn *c) {
    struct conn_elem *elem = malloc(sizeof (struct conn_elem));
    elem->conn = c;
    elem->next = NULL;

    if(!conn_l->head) {
        conn_l->head = elem;
        conn_l->tail = elem;
    }
    else {
        conn_l->tail->next = elem;
        conn_l->tail = elem;
    }
    conn_l->nconns++;
}

p9conn *rm_conn_by_fd(conn_list *conn_l, int fd) {
    p9conn *conn = NULL;

    struct conn_elem *current = conn_l->head;
    struct conn_elem *prev = NULL;
    while(current) {
        if(current->conn->fd == fd) {
            conn = current->conn;
            if(!prev) {
                // We're at head.
                if(conn_l->tail == conn_l->head) {
                    // Only one elem.
                    conn_l->head = NULL;
                    conn_l->tail = NULL;
                    free(current);
                    conn_l->nconns--;
                    return conn;
                }
                conn_l->head = current->next;
                free(current);
                conn_l->nconns--;
                return conn;
            }
            else {
                if(conn_l->tail == current) {
                    conn_l->tail = prev;
                }
                prev->next = current->next;
                conn_l->nconns--;
                return conn;
            }
        }
        prev = current;
        current = current->next;
    }
    return NULL;
}

size_t conn_count(conn_list *conn_l) {
    return conn_l->nconns;
}

p9conn *nth_conn(conn_list *conn_l, size_t nth) {
    if(nth >= conn_l->nconns) {
        return NULL;
    }
    struct conn_elem *elem = conn_l->head;
    for(size_t i = 0; i < nth; i++) {
        elem = elem->next;
    }
    return elem->conn;
}

int highest_conn_fd(conn_list *conn_l) {
    int highest = -1;
    struct conn_elem *current = conn_l->head;
    while(current) {
        if(current->conn->fd > highest) {
            highest = current->conn->fd;
        }
        current = current->next;
    }
    return highest;
}
