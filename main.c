#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "fcall.h"
#include "printer.h"
#include "bitset.h"
#include "9client.h"


void do_write(p9client *client);

int main(void)
{
    printf("Fcall size: %zu\n", sizeof (FCall));
    //p9client *client = p9_connect("9fs2", "564", "kyle", "");
    p9client *client = p9_connect("127.0.0.1", "9999", "kyle", "");
    //p9client *client = p9_connect("192.168.122.146", "564", "kyle", "");
    if(!client) {
        fprintf(stderr, "p9connect failed.\n");
        exit(1);
    }

    printf("------------------------------------\n");
    {
        p9fhandle fh = p9_duplicate(client, p9_get_root(client));
        printf("------------------------------------\n");

        if(!p9_open(client, fh, 0)) {
            printf("Failed to open root for read.\n");
            exit(1);
        }

        uint8_t dirdata[1024 * 8];
        uint8_t *current = dirdata;

        uint32_t count = p9_read(client, &fh, dirdata, 1024 * 8);

        printf("\n\ndirectory listing:\n");
        RStats rs = parse_stats(current, count);
        print_directory(rs);
        free_rstats(rs);
        p9_free_handle(client, fh);
        printf("------------------------------------\n");
    }

    // For local server
    {
        p9fhandle hello = p9_duplicate(client, p9_get_root(client));
        printf("------------------------------------\n");
        if(!p9_chdir(client, hello, "hello")) {
            printf("Failed to move fd to hello.\n");
            exit(1);
        }
        printf("------------------------------------\n");
        if(!p9_open(client, hello, 0)) {
            printf("Failed to change open hello for reading.\n");
            exit(1);
        }
        printf("------------------------------------\n");
        uint8_t hellodata[1024 * 8];

        size_t read;
        size_t readtotal = 0;
        while((read = p9_read(client, &hello, hellodata + readtotal, 1)) > 0) {
            readtotal += read;
        }
        printf("File: [\n");
        printf("%s", hellodata);
        printf("]\n");
    }
    exit(0);

    {

        p9fhandle kdir = p9_duplicate(client, p9_get_root(client));
        printf("------------------------------------\n");
        if(!p9_chdir(client, kdir, "usr/kyle")) {
            printf("Failed to change directory into usr/kyle.\n");
            exit(1);
        }

        printf("------------------------------------\n");
        if(!p9_open(client, kdir, 0)) {
            printf("Failed to open directory usr/kyle for reading.\n");
            exit(1);
        }

        printf("------------------------------------\n");
        uint8_t dirdata[1024 * 8];
        uint8_t *current = dirdata;
        int count = p9_read(client, &kdir, dirdata, 1024 * 8);

        printf("\n\ndirectory listing:\n");
        RStats rs = parse_stats(current, count);
        print_directory(rs);
        free_rstats(rs);
        p9_free_handle(client, kdir);
        printf("------------------------------------\n");
    }
    {
        p9fhandle p9rcfile = p9_duplicate(client, p9_get_root(client));
        printf("------------------------------------\n");
        p9_chdir(client, p9rcfile, "usr/kyle/plan9_rc_stuff.txt");
        //p9_chdir(client, p9rcfile, "usr/kyle/kyle.tgz");
        //p9_chdir(client, p9rcfile, "usr/kyle/golang.tar");
        //p9_chdir(client, p9rcfile, "usr/kyle/doesntexist.nothere");

        printf("------------------------------------\n");
        RStat *stat = p9_stat_file(client, p9rcfile);
        if(!stat) {
            printf("Failed to stat file.\n");
            exit(1);
        }

        printf("Reading file of length: %" PRIu64 "\n", stat->length);
        char *buffer = malloc(stat->length + 1);
        buffer[stat->length] = 0;
        printf("------------------------------------\n");
        p9_open(client, p9rcfile, 0);
        printf("------------------------------------\n");
        p9_read(client, &p9rcfile, (uint8_t *)buffer, stat->length);
        p9_free_stat(stat);

        printf("\nFile:[\n");
        printf("%s", buffer);
        printf("]\n\n");
        printf("------------------------------------\n");
        free(buffer);
        p9_free_handle(client, p9rcfile);
        printf("------------------------------------\n");
    }

    do_write(client);
    printf("Shutting down 9P2000 connection.\n");
    p9_shutdown(client);
}

void do_write(p9client *client) {
    p9fhandle testwrite = p9_duplicate(client, p9_get_root(client));
    printf("------------------------------------\n");
    p9_chdir(client, testwrite, "usr/kyle/testwrite");
    printf("------------------------------------\n");
    p9_open(client, testwrite, 1);
}
