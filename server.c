#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pwd.h>
#include <grp.h>

#include "9types.h"
#include "fcall.h"
#include "printer.h"
#include "9net.h"
#include "connection.h"
#include "filesystem.h"
#include "server.h"

enum ugo {
    ugo_user,
    ugo_group,
    ugo_other
};

struct request {
    FCall *fc;
    p9fs *fs;
    p9conn *conn;
};

int write_error(p9conn *conn, uint16_t tag, char *ename) {
    FCall retfc;
    retfc.tag = tag;
    retfc.type = Rerror;
    retfc.Rerror.ename = ename;
    printf("<<<\t");
    print_fcall(&retfc);
    return write_fcall(get_conn_fd(conn), &retfc);
}

int not_implemented(p9conn *conn, FCall *fc) {
    return write_error(conn, fc->tag, "Not implemented yet.");
}

int handle_Tversion(p9conn *conn, FCall *fc) {
    FCall retfc;
    retfc.type = Rversion;
    retfc.tag = fc->tag;
    retfc.TRversion.msize = fc->TRversion.msize;
    retfc.TRversion.version = fc->TRversion.version;
    printf("<<<\t");
    print_fcall(&retfc);
    return write_fcall(get_conn_fd(conn), &retfc);
}

int handle_Tattach(p9fs *fs, p9conn *conn, FCall *fc) {
    set_root_fid(conn, fc->Tattach.fid);

    FCall retfc;
    Qid q;
    q.type = (1 << 7);
    q.vers = 28;
    q.uid = 1;

    set_fs_user(fs, fc->Tattach.uname);

    retfc.type = Rattach;
    retfc.tag = fc->tag;
    retfc.Rattach.qid = q;
    printf("<<<\t");
    print_fcall(&retfc);
    return write_fcall(get_conn_fd(conn), &retfc);
}

int handle_Tstat(p9fs *fs, p9conn *conn, FCall *fc) {
    p9file *file = file_for_fid(fs, conn, fc->Tstat.fid);
    if(!file) {
        return write_error(conn, fc->tag, "File not found.");
    }

    RStat *stat = &file->rstat;

    FCall retfc;
    retfc.type = Rstat;
    retfc.tag = fc->tag;
    copy_rstat(&retfc.Rstat, stat);

    printf("<<<\t");
    print_fcall(&retfc);
    int ret = write_fcall(get_conn_fd(conn), &retfc);
    free_rstat(&retfc.Rstat);
    return ret;
}

int handle_Twalk(p9fs *fs, p9conn *conn, FCall *fc) {
    p9file *file = file_for_fid(fs, conn, fc->Twalk.fid);
    if(!file) {
        return write_error(conn, fc->tag, "Invalid fid.");
    }

    uint16_t i;
    size_t total_len = strlen(file->name) + 1;
    for(i = 0; i < fc->Twalk.nwname; i++) {
        total_len += strlen(fc->Twalk.wname[i]) + 1;
    }

    FCall retfc;
    retfc.Rwalk.nwqid = 0;
    uint16_t find_qids = 1;
    char *newpath = malloc(total_len);
    strcpy(newpath, file->name);

    for(i = 0; i < fc->Twalk.nwname; i++) {
        char buffer[256];
        if(newpath[strlen(newpath) - 1] == '/') {
            sprintf(buffer, "%s", fc->Twalk.wname[i]);
        }
        else {
            sprintf(buffer, "/%s", fc->Twalk.wname[i]);
        }
        strcat(newpath, buffer);

        p9file *file;
        if((file = fs_get_file(fs, newpath)) == NULL) {
            printf("Could not find file %s\n", newpath);
            find_qids = 0;
        }
        if(find_qids) {
            retfc.Rwalk.wqid[retfc.Rwalk.nwqid++] = file->rstat.qid;
        }
    }

    if(file_for_fid(fs, conn, fc->Twalk.newfid)) {
        // Something has gone wrong. Client is trying to create
        // a new fid, but that new fid already exists.

        // Right now, this code frees the existing FID and
        // replaces it with the new one. This may not be the
        // correct behavior.
        free_fid(conn, fc->Twalk.newfid);
    }
    set_fid_path(conn, fc->Twalk.newfid, newpath);


    retfc.type = Rwalk;
    retfc.tag = fc->tag;
    printf("<<<\t");
    print_fcall(&retfc);
    return write_fcall(get_conn_fd(conn), &retfc);
}

int user_in_group(const char *user, const char *group) {
    struct passwd *pw = getpwnam(user);
    if(!pw) return 0;


    int ngroups = 0;

    // Find number of groups user is in.
    if (getgrouplist(user, pw->pw_gid, NULL, &ngroups) == -1) {
        return 0;
    }

    gid_t *groups = malloc(ngroups * sizeof (gid_t));
    if(getgrouplist(user, pw->pw_gid, groups, &ngroups) == -1) {
        return 0;
    }

    for(int i = 0; i < ngroups; i++) {
        struct group *g = getgrgid(groups[i]);
        if(strcmp(g->gr_name, group) == 0) {
            return 1;
        }
    }
    return 0;
}

enum ugo user_relation(const char *fsuser, p9file *file) {
    if(strcmp(fsuser, file->rstat.uid) == 0) {
        // user is owner.
        return ugo_user;
    }

    if(user_in_group(fsuser, file->rstat.gid)) {
        return ugo_group;
    }

    return ugo_other;
}

int omode_permits(uint8_t perm, uint8_t omode) {

    switch(omode) {
    case oread:
        printf("opening for read. %u\n", perm);
        return perm & 0x04;
        break;
    case owrite:
        printf("opening for write.\n");
        return perm & 0x02;
        break;
    case ordwr:
        printf("opening for read and write.\n");
        return perm & 0x06;
        break;
    case oexec:
        printf("opening for execute.\n");
        return perm & 0x01;
        break;
    case none:
        printf("opening for none.\n");
        return 0;
        break;
    default:
        printf("opening for unknown mode.\n");
        return 0;
        break;
    }
}

int open_permission(const char *fsuser, p9file *file, uint8_t omode) {
    switch(user_relation(fsuser, file)) {
    case ugo_user:
        printf("FSUser is user ");
        return omode_permits((file->rstat.mode >> 6) & 0x07, omode);
        break;
    case ugo_group:
        printf("FSUser is in group ");
        return omode_permits((file->rstat.mode >> 3) & 0x07, omode);
        break;
    case ugo_other:
        printf("FSUser is other ");
        return omode_permits(file->rstat.mode & 0x07, omode);
        break;
    }
}

int handle_Topen(p9server *server, p9fs *fs, p9conn *conn, FCall *fc) {
    p9file *file = file_for_fid(fs, conn, fc->Topen.fid);
    if(!file) {
        return write_error(conn, fc->tag, "Invalid fid.");
    }
    uint8_t openmode = get_fid_openmode(conn, fc->Topen.fid);

    if(openmode != none) {
        return write_error(conn, fc->tag, "File already open.");
    }
    if(!open_permission(get_fs_user(fs), file, fc->Topen.mode & 0x0F)) {
        return write_error(conn, fc->tag, "Permission denied.");
    }

    // We're opening a directory. Rewrite the rstat contents.
    // Doing this means we don't have to rewrite them when
    // we modify the directory's contents.
    if(file->rstat.mode & (1 << 31)) {
        open_file(fs, conn, fc->Topen.fid, fc->Topen.mode);

        for(size_t i = 0; i < file->subfile_n; i++) {
            printf("subfile: %s\n", file->subfiles[i]->name);
        }

        fs_write_dir_rstats(fs, file);
        FCall retfc;
        retfc.type = Ropen;
        retfc.tag = fc->tag;
        retfc.Ropen.qid = file->rstat.qid;
        retfc.Ropen.iounit = IOUNIT;
        printf("<<<\t");
        print_fcall(&retfc);
        return write_fcall(get_conn_fd(conn), &retfc);
    }
    else {
        request req;
        req.fc = fc;
        req.fs = fs;
        req.conn = conn;
        if(server->open) {
            server->open(&req, file);
            return 1;
        }
        return write_error(conn, fc->tag, "Cannot open file.");
    }
}

int handle_Tread(p9server *server, p9fs *fs, p9conn *conn, FCall *fc) {
    if(fc->Tread.count > 8168) {
        return write_error(conn, fc->tag, "Read size too large.");
    }
    p9file *file = file_for_fid(fs, conn, fc->Tread.fid);
    if(!file) {
        return write_error(conn, fc->tag, "Failed to read from FID.");
    }
    uint8_t openmode = get_fid_openmode(conn, fc->Tread.fid);

    if((openmode & 0x0F) != oread
            && (openmode & 0x0F) != ordwr
            && (openmode & 0x0F) != oexec) {
        return write_error(conn, fc->tag, "File not opened for read.");
    }

    uint32_t count = fc->Tread.offset + fc->Tread.count > file->rstat.length ? file->rstat.length - fc->Tread.offset : fc->Tread.count;

    if(file->rstat.mode & (1<<31)) {
        printf("Writing a directory.\n");
        // Directory.
        FCall retfc;
        uint8_t data[count];

        memcpy(data, file->contents+fc->Tread.offset, count);

        retfc.tag = fc->tag;
        retfc.type = Rread;
        retfc.Rread.count = count;
        retfc.Rread.data = data;
        printf("<<<\t");
        print_fcall(&retfc);
        return write_fcall(get_conn_fd(conn), &retfc);
    }

    request req;
    req.fc = fc;
    req.fs = fs;
    req.conn = conn;
    if(server->read) {
        server->read(&req, file, fc->Tread.offset, count);
        return 1;
    }
    return write_error(conn, fc->tag, "Cannot read from file.");

}

int handle_Tclunk(p9fs *fs, p9conn *conn, FCall *fc) {
    free_fid(conn, fc->Tclunk.fid);

    FCall retfc;
    retfc.tag = fc->tag;
    retfc.type = Rclunk;
    printf("<<<\t");
    print_fcall(&retfc);
    return write_fcall(get_conn_fd(conn), &retfc);
}

p9fs *fs;

int handle_Tcreate(p9server *server, p9fs *fs, p9conn *conn, FCall *fc) {
    //RStat stat;
    //stat.type = 0;
    //stat.dev = 0;
    //stat.qid = alloc_qid(fs, fc->Tcreate.perm >> 24);
    //stat.mode = fc->Tcreate.perm;
    //stat.atime = time(NULL);
    //stat.mtime = time(NULL);
    //stat.length = 0;
    //char *namebuff = malloc(strlen(fc->Tcreate.name) + 1); // null mallocs?
    //uint8_t *content = NULL; // will this blow up?

    //strcpy(namebuff, fc->Tcreate.name); // null fc->Tcreate.name?
    //stat.name = namebuff;
    //char *uname = strdup(get_fs_user(fs));
    //stat.uid = uname;
    //stat.gid = uname;
    //stat.muid = uname;

    p9file *dir = file_for_fid(fs, conn, fc->Tcreate.fid);
    if(!dir) {
        //free(namebuff);
        printf("Failed to get directory for fid %u\n", fc->Tcreate.fid);
        return write_error(conn, fc->tag, "Failed to find dir.");
    }
//    char *name = dir->name;
//    char *dirbuff = malloc(strlen(name) + 1 + strlen(namebuff) + 1);
//    strcpy(dirbuff, name);
//    if(dirbuff[strlen(name)-1] != '/') {
//        strcat(dirbuff, "/");
//    }
//    strcat(dirbuff, namebuff);
//    printf("Adding file: %s\n", dirbuff);

    request req;
    req.fc = fc;
    req.fs = fs;
    req.conn = conn;
    if(server->create) {
        server->create(&req, dir, fc->Tcreate.name);
        return 1;
    }
    return write_error(conn, fc->tag, "Cannot create files.");
//    p9file *newf = add_file(fs, dirbuff, content, &stat);
//    free(namebuff);
//    free(uname);
//    if(!newf) {
//        free(dirbuff);
//        dump_files(fs);
//        return write_error(conn, fc->tag, "File limit reached.");
//        //exit(1);
//    }
//
//    char *fidbuff = malloc(strlen(dirbuff) + 1);
//    strcpy(fidbuff, dirbuff);
//    set_fid_path(conn, fc->Tcreate.fid, fidbuff);
//    set_fid_openmode(conn, fc->Tcreate.fid, ordwr);
//
//    fs_add_subfile(fs, dir, newf);
//
//
//    FCall retfc;
//    retfc.tag = fc->tag;
//    retfc.type = Rcreate;
//    retfc.Rcreate.qid = stat.qid;
//    retfc.Rcreate.iounit = IOUNIT;
//    printf("<<<\t");
//    print_fcall(&retfc);
//    return write_fcall(get_conn_fd(conn), &retfc);
}

int handle_Twstat(p9fs *fs, p9conn *conn, FCall *fc) {
    p9file *file = file_for_fid(fs, conn, fc->Twstat.fid);
    if(!file) {
        return write_error(conn, fc->tag, "File not found.");
    }

    /* The name can be changed by anyone with write permission in
     * the parent directory; it is an error to change the name to
     * that of an existing file.
     *
     * The length can be changed (affecting the actual length of
     * the file) by anyone with write permission on the file. It
     * is an error to attempt to set the length of a directory to
     * a non-zero value, and servers may decide to reject length
     * changes for other reasons.
     *
     * The mode and mtime can be changed by the owner of the file
     * or the group leader of the file's current group.
     *
     * The directory bit cannot be changed by a wstat;
     *
     * the other defined permission and mode bits can.
     *
     * The gid can be changed: by the owner if also a member of
     * the new group; or by the group leader of the file's current
     * group if also leader of the new group (see intro(5) for
     * more information about permissions and users(6) for users
     * and groups)
     */

    /* A wstat request can avoid modifying some properties of the
     * file by providing explicit ``don't touch'' values in the
     * stat data that is sent: zero-length strings for text
     * values and the maximum unsigned value of appropriate size
     * for inte- gral values. As a special case, if all the
     * elements of the directory entry in a Twstat message are
     * ``don't touch'' val- ues, the server may interpret it as a
     * request to guarantee that the contents of the associated
     * file are committed to stable storage before the Rwstat
     * message is returned. (Con- sider the message to mean,
     * ``make the state of the file exactly what it claims to be.'')
     */

    RStat *fstat = &file->rstat;
    RStat *nstat = &fc->Twstat.stat;

    enum ugo relation = user_relation(get_fs_user(fs), file);

    if(strlen(nstat->name) != 0) {
        if(relation != ugo_user) {
            return write_error(conn, fc->tag, "Permission denied.");
        }
//        Need to assemble path and check if file exists.
//        if(fs_get_file(...) != NULL) {
//            return write_error(...);
//        }
        fstat->name = realloc(fstat->name,  strlen(nstat->name) + 1);
        strcpy(fstat->name, nstat->name);
    }

    if(nstat->length != ~0) {
        if(!open_permission(get_fs_user(fs), file, owrite)) {
            return write_error(conn, fc->tag, "Permission denied.");
        }

        fstat->length = nstat->length;
        if(!file->contents) {
            file->contents = malloc(nstat->length);
        }
        else {
            file->contents = realloc(file->contents, nstat->length); // null alloc?
        }
    }

    if(nstat->mode != ~0) {
        if(relation != ugo_user) {
            return write_error(conn, fc->tag, "Permission denied.");
        }

        // Only grab low 9 bits for settable permissions.
        uint32_t newmode = nstat->mode & 0x000001FF;
        fstat->mode = (fstat->mode & ~(0x1FF)) | newmode;
    }

    if(nstat->mtime != ~0) {
        if(relation != ugo_user) {
            return write_error(conn, fc->tag, "Permission denied.");
        }

        fstat->mtime = nstat->mtime;
    }

    if(strlen(nstat->gid) != 0) {
        fstat->gid = realloc(fstat->gid, strlen(nstat->gid) + 1);
        strcpy(fstat->gid, nstat->gid);
    }

    FCall retfc;
    retfc.type = Rwstat;
    retfc.tag = fc->tag;

    printf("<<<\t");
    print_fcall(&retfc);
    return write_fcall(get_conn_fd(conn), &retfc);
}

int handle_Twrite(p9server *server, p9fs *fs, p9conn *conn, FCall *fc) {
    p9file *file = file_for_fid(fs, conn, fc->Twrite.fid);
    if(!file) {
        return write_error(conn, fc->tag, "File not found.");
    }
    uint8_t openmode = get_fid_openmode(conn, fc->Twrite.fid);

    if((openmode & 0x0F) != owrite
            && (openmode & 0x0F) != ordwr) {
        printf("Actual mode: %u\n", openmode);
        return write_error(conn, fc->tag, "File not opened for write.");
    }
    else if(file->rstat.mode & (1 << 31)) {
        return write_error(conn, fc->tag, "Cannot write to directory.");
    }

    uint64_t offset = fc->Twrite.offset;
    if(!(openmode & 0x10)) {
        // If we're not truncating, offset should be current length.
        uint32_t foffset = get_fid_openoffset(conn, fc->Twrite.fid);
        offset += foffset;
    }

    if(offset + fc->Twrite.count > file->rstat.length) {
        // File's buffer not big enough. Reallocate.
        size_t newlen = offset + fc->Twrite.count;
        file->contents = realloc(file->contents, newlen);
        file->rstat.length = newlen;
    }
    request req;
    req.fc = fc;
    req.fs = fs;
    req.conn = conn;
    if(server->write) {
        server->write(&req, file, fc->Twrite.data, offset, fc->Twrite.count);
        return 1;
    }
    return write_error(conn, fc->tag, "Cannot write to file.");
//    memcpy(file->contents + offset, fc->Twrite.data, fc->Twrite.count);
//
//    FCall retfc;
//    retfc.type = Rwrite;
//    retfc.tag = fc->tag;
//    retfc.Rwrite.count = fc->Twrite.count;
//
//    printf("<<<\t");
//    print_fcall(&retfc);
//    return write_fcall(get_conn_fd(conn), &retfc);

}

int handle_Tremove(p9fs *fs, p9conn *conn, FCall *fc) {
    p9file *file = file_for_fid(fs, conn, fc->Twrite.fid);
    if(!file) {
        return write_error(conn, fc->tag, "File not found.");
    }
    if(!open_permission(get_fs_user(fs), file, owrite)) {
        return write_error(conn, fc->tag, "Permission denied.");
    }

    delfile(fs, conn, fc->Tremove.fid);
    FCall retfc;
    retfc.type = Rremove;
    retfc.tag = fc->tag;

    printf("<<<\t");
    print_fcall(&retfc);
    return write_fcall(get_conn_fd(conn), &retfc);
}

void setup_files(p9fs *fs) {
    uint32_t mode = 0;
    for(int i = 0; i < 9; i++) {
        mode |= (1<<i);
    }
    mode = mode ^ (1 << 1); // o-w

    RStat stat;
    stat.type = 0;
    stat.dev = 0;
    alloc_qid(fs, 0);
    stat.mode = mode;
    stat.atime = time(NULL);
    stat.mtime = time(NULL);
    stat.length = 14;
    stat.name = "hello";
    stat.uid = "root";
    stat.gid = "root";
    stat.muid = "root";

    char *hellobuff = "hello world\n";
    uint8_t *h1buff = malloc(strlen(hellobuff) + 1);
    strcpy((char *)h1buff, hellobuff);
    //p9file *h1 = add_file(fs, strdup("/hello"), h1buff, &stat);

    stat.name = "hello2";
    stat.qid = alloc_qid(fs, 0);
    uint8_t *h2buff = malloc(strlen(hellobuff) + 1);
    strcpy((char *)h2buff, hellobuff);
    //p9file *h2 = add_file(fs, strdup("/hello2"), h2buff, &stat);

    stat.qid = alloc_qid(fs, 1 << 7);
    stat.mode = mode | (1 << 31) | (1 << 1); // Add dir bit and o+w
    stat.length = 0;
    stat.name = "/";

    p9file *root = fs_add_file(fs, strdup("/"), NULL, &stat);

    //fs_add_subfile(fs, root, h1);
    //fs_add_subfile(fs, root, h2);
    fs_write_dir_rstats(fs, root);
}

int serve(p9server *p9srv) {
    fs = create_fs();

    setup_files(fs);

    p9file *file = fs_get_file(fs, "/");

    if(p9srv->setup) {
        p9srv->setup(p9srv, fs, file);
    }

    FCall *fc;
    int fd = p9net_listen_on("0.0.0.0", "9999");

    conn_list *conns = create_conn_list();

    while(1) {
        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(fd, &readfds);

        for(size_t i = 0; i < conn_count(conns); i++) {
            FD_SET(get_conn_fd(nth_conn(conns, i)), &readfds);
        }
        int nfds = highest_conn_fd(conns);
        if(nfds == -1) {
            nfds = fd + 1;
        }
        else {
            nfds++;
        }
        printf("Selecting on nfds: %d\n", nfds);
        select(nfds, &readfds, NULL, NULL, NULL);

        if(FD_ISSET(fd, &readfds)) {
            int newconn_fd = accept(fd, NULL, NULL);
            if(newconn_fd == -1) {
                break;
            }
            printf("Adding new connection on FD: %d\n", newconn_fd);
            p9conn *newconn = create_p9conn(newconn_fd);
            add_conn(conns, newconn);
        }

        for(size_t i = 0; i < conn_count(conns); i++) {
            p9conn *conn = nth_conn(conns, i);
            int cfd = get_conn_fd(conn);

            if(!FD_ISSET(cfd, &readfds)) {
                continue;
            }

            fc = parse_call(cfd);
            if(!fc) {
                printf("Failed to parse message from client.\n");
                rm_conn_by_fd(conns, cfd);
                close(cfd);
                continue;
            }
            printf(">>>%d\t", cfd);
            print_fcall(fc);

            int ret = 1;
            switch(fc->type) {
            case Tversion:
                ret = handle_Tversion(conn, fc);
                break;
            case Tattach:
                ret = handle_Tattach(fs, conn, fc);
                break;
            case Tstat:
                ret = handle_Tstat(fs, conn, fc);
                break;
            case Twalk:
                ret = handle_Twalk(fs, conn, fc);
                break;
            case Topen:
                ret = handle_Topen(p9srv, fs, conn, fc);
                break;
            case Tread:
                ret = handle_Tread(p9srv, fs, conn, fc);
                break;
            case Tclunk:
                ret = handle_Tclunk(fs, conn, fc);
                break;
            case Tcreate:
                ret = handle_Tcreate(p9srv, fs, conn, fc);
                break;
            case Twstat:
                ret = handle_Twstat(fs, conn, fc);
                break;
            case Twrite:
                ret = handle_Twrite(p9srv, fs, conn, fc);
                break;
            case Tremove:
                ret = handle_Tremove(fs, conn, fc);
                break;
            default:
                not_implemented(conn, fc);
                break;
            }
            free_call(fc);
            if(!ret) {
                printf("Failed to write response.\n");
                break;
            }
        }
        //destroy_p9conn(conn);
        //close(cfd);

        printf("looping.\n");
        //break;
    }
    destroy_fs(fs);
    close(fd);
    return 0;
}

void respond(const request *req, response *r) {
    p9file *file;
    FCall retfc;
    RStat stat;
    if(r != NULL) {
        switch(req->fc->type) {
        case Topen:
             file = open_file(req->fs, req->conn, req->fc->Topen.fid, req->fc->Topen.mode);
            if(!file) {
                write_error(req->conn, req->fc->tag, "File not found.");
            }
            retfc.type = Ropen;
            retfc.tag = req->fc->tag;
            retfc.Ropen.qid = file->rstat.qid;
            retfc.Ropen.iounit = IOUNIT;
            printf("<<<\t");
            print_fcall(&retfc);
            write_fcall(get_conn_fd(req->conn), &retfc);
            break;
        case Tread:
            //uint8_t data[r->read_response.count];
            //memcpy(data, r->buffer, r->read_response.count);
            retfc.tag = req->fc->tag;
            retfc.type = Rread;
            retfc.Rread.count = r->read_response.count;
            retfc.Rread.data = r->read_response.data;
            printf("<<<\t");
            print_fcall(&retfc);
            write_fcall(get_conn_fd(req->conn), &retfc);
            break;
        case Twrite:
            retfc.type = Rwrite;
            retfc.tag = req->fc->tag;
            retfc.Rwrite.count = r->write_response.count;

            printf("<<<\t");
            print_fcall(&retfc);
            write_fcall(get_conn_fd(req->conn), &retfc);
            break;
        case Tcreate:
            stat.type = 0;
            stat.dev = 0;
            alloc_qid(fs, 0);
            stat.mode = req->fc->Tcreate.perm;
            stat.atime = time(NULL);
            stat.mtime = time(NULL);
            stat.length = r->create_response.length;
            stat.name = r->create_response.fname; //"hello";
            stat.uid = r->create_response.uid; //"root";
            stat.gid = r->create_response.gid; //"root";
            stat.muid = r->create_response.uid;

            p9file *dir = file_for_fid(req->fs, req->conn, req->fc->Tcreate.fid);
            char *name = dir->name;
            char *dirbuff = malloc(strlen(name) + 1 + strlen(r->create_response.fname) + 1);
            strcpy(dirbuff, name);
            if(dirbuff[strlen(name)-1] != '/') {
                strcat(dirbuff, "/");
            }
            strcat(dirbuff, r->create_response.fname);

            printf("Adding file %s\n", dirbuff);
            file = fs_add_file(req->fs, dirbuff, NULL, &stat);
            if(!file) {
                write_error(req->conn, req->fc->tag, "Failed to create file.");
            }
//            char *fidbuff = malloc(strlen(r->create_response.fname) + 1);
//            strcpy(fidbuff, r->create_response.fname);
            set_fid_path(req->conn, req->fc->Tcreate.fid, dirbuff);
            set_fid_openmode(req->conn, req->fc->Tcreate.fid, ordwr);
            fs_add_subfile(req->fs, dir, file);

            retfc.tag = req->fc->tag;
            retfc.type = Rcreate;
            retfc.Rcreate.qid = stat.qid;
            retfc.Rcreate.iounit = IOUNIT;
            printf("<<<\t");
            print_fcall(&retfc);
            write_fcall(get_conn_fd(req->conn), &retfc);
            break;
        }
    }
    else {
        switch(req->fc->type) {
        case Topen:
            write_error(req->conn, req->fc->tag, "Failed to open file.");
            break;
        case Tread:
            write_error(req->conn, req->fc->tag, "Failed to read file.");
            break;
        case Twrite:
            write_error(req->conn, req->fc->tag, "Failed to write file.");
            break;
        case Tcreate:
            write_error(req->conn, req->fc->tag, "Failed to create file.");
            break;
        }
    }
}

p9file *add_file(p9server *p9srv, p9fs *fs, p9file *parent, char *fname, char *uid, char * gid, size_t length) {
    if(!parent) {
        return NULL;
    }
    RStat stat;
    stat.type = 0;
    stat.dev = 0;
    alloc_qid(fs, 0);
    stat.mode = p9srv->default_mode;
    stat.atime = time(NULL);
    stat.mtime = time(NULL);
    stat.length = length;
    stat.name = strdup(fname);
    stat.uid = uid;
    stat.gid = gid;
    stat.muid = uid;

    char *namebuff = malloc(sizeof(parent->name) + 1 + sizeof(fname) + 1);
    if(strcmp(parent->name, "/") == 0) {
        strcpy(namebuff, parent->name);
        strcat(namebuff, fname);
    }
    else {
        strcpy(namebuff, parent->name);
        strcat(namebuff, "/");
        strcat(namebuff, fname);
    }

    printf("Adding file %s\n", namebuff);
    p9file *newf = fs_add_file(fs, namebuff, NULL, &stat);
    fs_add_subfile(fs, parent, newf);
    return newf;
}

p9file *add_dir(p9server *p9srv, p9fs *fs, p9file *parent, char *dname, char *uid, char * gid) {
    if(!parent) {
        return NULL;
    }
    RStat stat;
    stat.type = 0;
    stat.dev = 0;
    stat.qid = alloc_qid(fs, 1 << 7);
    stat.mode = p9srv->default_mode | (1<<31);
    stat.atime = time(NULL);
    stat.mtime = time(NULL);
    stat.length = 0;
    stat.name = strdup(dname);
    stat.uid = uid;
    stat.gid = gid;
    stat.muid = uid;

    char *namebuff = malloc(sizeof(parent->name) + 1 + sizeof(dname) + 1);
    if(strcmp(parent->name, "/") == 0) {
        strcpy(namebuff, parent->name);
        strcat(namebuff, dname);
    }
    else {
        strcpy(namebuff, parent->name);
        strcat(namebuff, "/");
        strcat(namebuff, dname);
    }

    printf("Adding directory %s\n", namebuff);
    p9file *newf = fs_add_file(fs, namebuff, NULL, &stat);
    if(parent) {
        fs_add_subfile(fs, parent, newf);
    }
    return newf;
}
