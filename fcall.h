#ifndef FCALL_H
#define FCALL_H

/**
 *    size[4] Tversion tag[2] msize[4] version[s]
 *    size[4] Rversion tag[2] msize[4] version[s]
 *
 *    size[4] Tauth tag[2] afid[4] uname[s] aname[s]
 *    size[4] Rauth tag[2] aqid[13]
 *
 *    size[4] Rerror tag[2] ename[s]
 *
 *    size[4] Tflush tag[2] oldtag[2]
 *    size[4] Rflush tag[2]
 *
 *    size[4] Tattach tag[2] fid[4] afid[4] uname[s] aname[s]
 *    size[4] Rattach tag[2] qid[13]
 *
 *    size[4] Twalk tag[2] fid[4] newfid[4] nwname[2] nwname*(wname[s])
 *    size[4] Rwalk tag[2] nwqid[2] nwqid*(wqid[13])
 *
 *    size[4] Topen tag[2] fid[4] mode[1]
 *    size[4] Ropen tag[2] qid[13] iounit[4]
 *
 *    size[4] Tcreate tag[2] fid[4] name[s] perm[4] mode[1]
 *    size[4] Rcreate tag[2] qid[13] iounit[4]
 *
 *    size[4] Tread tag[2] fid[4] offset[8] count[4]
 *    size[4] Rread tag[2] count[4] data[count]
 *
 *    size[4] Twrite tag[2] fid[4] offset[8] count[4] data[count]
 *    size[4] Rwrite tag[2] count[4]
 *
 *    size[4] Tclunk tag[2] fid[4]
 *    size[4] Rclunk tag[2]
 *
 *    size[4] Tremove tag[2] fid[4]
 *    size[4] Rremove tag[2]
 *
 *    size[4] Tstat tag[2] fid[4]
 *    size[4] Rstat tag[2] stat[n]
 *
 *    size[4] Twstat tag[2] fid[4] stat[n]
 *    size[4] Rwstat tag[2]
 */

#define MAXWELEM 16

typedef struct {
    uint8_t type;
    uint32_t vers;
    uint64_t uid;
} Qid;

typedef struct {
    uint32_t msize;
    char *version;
} TRVersion;

typedef struct {
    uint32_t afid;
    char *uname;
    char *aname;
} TAuth;

typedef struct {
    Qid aqid;
} RAuth;

typedef struct {
    char *ename;
} RError;

typedef struct {
    uint16_t oldtag;
} TFlush;

typedef struct {
    uint32_t fid;
    uint32_t afid;
    char *uname;
    char *aname;
} TAttach;

typedef struct {
    Qid qid;
} RAttach;

typedef struct {
    uint32_t fid;
    uint32_t newfid;
    uint16_t nwname;
    char *wname[MAXWELEM];
} TWalk;

typedef struct {
    uint16_t nwqid;
    Qid wqid[MAXWELEM];
} RWalk;

typedef struct {
    uint32_t fid;
    uint8_t mode;
} TOpen;

typedef struct {
    Qid qid;
    uint32_t iounit;
} ROpen;

typedef struct {
    uint32_t fid;
    char *name;
    uint32_t perm;
    uint8_t mode;
} TCreate;

typedef struct {
    Qid qid;
    uint32_t iounit;
} RCreate;

typedef struct {
    uint32_t fid;
    uint64_t offset;
    uint32_t count;
} TRead;

typedef struct {
    uint32_t count;
    uint8_t *data;
} RRead;

typedef struct {
    uint32_t fid;
    uint64_t offset;
    uint32_t count;
    uint8_t *data;
} TWrite;

typedef struct {
    uint32_t count;
} RWrite;

typedef struct {
    uint32_t fid;
} TClunk;

typedef struct {
    uint32_t fid;
} TRemove;

typedef struct {
    uint32_t fid;
} TStat;

typedef struct {
    uint16_t type;
    uint32_t dev;
    Qid qid;
    uint32_t mode;
    uint32_t atime;
    uint32_t mtime;
    uint64_t length;
    char *name;
    char *uid;
    char *gid;
    char *muid;
} RStat;

typedef struct {
    size_t stat_count;
    RStat *stats;
} RStats;

typedef struct {
    uint32_t fid;
    RStat stat;
} TWstat;

typedef struct {
    uint8_t type;
    uint16_t tag;

    union {
        // Tversion, Rversion
        TRVersion TRversion;
        TAuth Tauth;
        RAuth Rauth;
        RError Rerror;
        TFlush Tflush;
        TAttach Tattach;
        RAttach Rattach;
        TWalk Twalk;
        RWalk Rwalk;
        TOpen Topen;
        ROpen Ropen;
        TCreate Tcreate;
        RCreate Rcreate;
        TRead Tread;
        RRead Rread;
        TWrite Twrite;
        RWrite Rwrite;
        TClunk Tclunk;
        TRemove Tremove;
        TStat Tstat;
        RStat Rstat;
        TWstat Twstat;
    };
} FCall;

FCall *parse_call(int sockfd);
void free_call(FCall *fc);

RStats parse_stats(uint8_t *buffer, uint32_t count);
void free_rstats(RStats stats);
void parse_stat(RStat *stat, uint8_t *messagep, uint8_t **newmessage);
void free_rstat(RStat *rs);
void copy_rstat(RStat *dst, RStat *src);
void write_rstat(RStat *stat, uint8_t *buffer, uint8_t **newbuffer);
uint16_t stat_length(RStat *rs);

int write_fcall(int sockfd, FCall *fc);


#endif
