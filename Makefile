MAKEFLAGS=-j10

CC=clang
CFLAGS=-g -O0 -Wall -Werror

LDFLAGS = #-lefence

CLIENT=testclient
SERVER=testserver

COBJECTS=9types.o\
	9net.o\
	fcall.o\
	printer.o\
	reader.o\
	9client.o\
	bitset.o\
	main.o

SOBJECTS=9types.o\
	9net.o\
	fcall.o\
	printer.o\
	reader.o\
	servmain.o\
	filesystem.o\
	connection.o\
	server.o\


all: $(CLIENT) $(SERVER)

$(CLIENT) : $(COBJECTS)
	-@echo -e [LD] '\t' $(COBJECTS) '->' $(CLIENT)
	@$(CC) $(CFLAGS) $(LDFLAGS) $(COBJECTS) -o $(CLIENT)

$(SERVER) : $(SOBJECTS)
	-@echo -e [LD] '\t' $(SOBJECTS) '->' $(SERVER)
	@$(CC) $(CFLAGS) $(LDFLAGS) $(SOBJECTS) -o $(SERVER)

%.o : %.c %.h
	-@echo -e [CC] '\t' $@
	@$(CC) $(CFLAGS) -o $@ -c $<

%.o : %.c
	-@echo -e [CC] '\t' $@
	@$(CC) $(CFLAGS) -o $@ -c $<


clean:
	-@rm *~ *.o

nuke: clean
	-@rm $(CLIENT) $(SERVER)
